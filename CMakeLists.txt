cmake_minimum_required(VERSION 3.16)

project(rp_simulator VERSION 1.3.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)

# Add Qt
find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets)

# Add Gtest
find_package(GTest REQUIRED)

# Add threads
find_package(Threads REQUIRED)

# My modules
# Core
file(GLOB core_src src/core/*.cpp)
add_library(core STATIC ${core_src})

# Interface
file(GLOB interface_src src/interface/*.cpp)
add_library(interface STATIC ${interface_src})

# Runtime
file(GLOB runtime_src src/runtime/*.cpp)
add_library(runtime STATIC ${runtime_src})

# Script (currently empty)


# My executables
add_executable(simulator src/main.cpp)

add_executable(tests src/tests.cpp)

# Dependencies
target_link_libraries(runtime PUBLIC core)

target_link_libraries(interface PUBLIC runtime)
target_include_directories(interface PUBLIC ${Qt6_INCLUDE_DIRS})
target_link_libraries(interface PUBLIC Qt6::Core Qt6::Gui Qt6::Widgets)

target_link_libraries(simulator PRIVATE interface)

target_link_libraries(runtime PUBLIC ${Threads_LIBRARY})
target_link_libraries(tests PRIVATE runtime)
target_link_libraries(tests PRIVATE core)
target_link_libraries(tests PRIVATE GTest::gtest GTest::gtest_main)

# Other stuff
set_target_properties(simulator PROPERTIES WIN32_EXECUTABLE ON MACOSX_BUNDLE ON)

