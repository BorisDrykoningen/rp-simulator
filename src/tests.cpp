#include <gtest/gtest.h>

#include "core/attribute.hpp"
#include "core/character.hpp"
#include "core/heads.hpp"
#include "core/simulation.hpp"
#include "core/skills.hpp"
#include "core/statistics.hpp"
#include "runtime/threaded_runtime.hpp"

#include <future>


TEST(Attribute, NoModsNoLimits) {
    const Attribute a(42);
    EXPECT_EQ(a.get(), 42);
    EXPECT_EQ(a.getValue(), 42);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);
}


TEST(Attribute, NoMods) {
    const Attribute a(-5, 0, 5);
    EXPECT_EQ(a.get(), 0);
    EXPECT_EQ(a.getValue(), 0);
    EXPECT_EQ(a.getMin(), -5);
    EXPECT_EQ(a.getMax(), 5);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);
}


TEST(Attribute, OutOfPositiveBoundNoLimits) {
    constexpr Attribute::IntType buff = std::numeric_limits<Attribute::IntType>::max() - 30;
    Attribute a(42);
    a.buff(buff);

    EXPECT_EQ(a.get(), std::numeric_limits<Attribute::IntType>::max());
    EXPECT_EQ(a.getValue(), 42);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), buff);
    EXPECT_EQ(a.getEffectiveModifier(), buff - 12);
}


TEST(Attribute, OutOfNegativeBoundNoLimits) {
    constexpr Attribute::IntType nerf = -(std::numeric_limits<Attribute::IntType>::min() + 30);
    Attribute a(-42);
    a.nerf(nerf);

    EXPECT_EQ(a.get(), std::numeric_limits<Attribute::IntType>::min());
    EXPECT_EQ(a.getValue(), -42);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), -nerf);
    EXPECT_EQ(a.getEffectiveModifier(), -(nerf - 12));
}


TEST(Attribute, OutOfPositiveLimit) {
    Attribute a(-10, 5, 10);
    a.buff(7);

    EXPECT_EQ(a.get(), 10);
    EXPECT_EQ(a.getValue(), 5);
    EXPECT_EQ(a.getMin(), -10);
    EXPECT_EQ(a.getMax(), 10);
    EXPECT_EQ(a.getTrueModifier(), 7);
    EXPECT_EQ(a.getEffectiveModifier(), 5);
}


TEST(Attribute, OutOfNegativeLimit) {
    Attribute a(-10, -5, 10);
    a.nerf(7);

    EXPECT_EQ(a.get(), -10);
    EXPECT_EQ(a.getValue(), -5);
    EXPECT_EQ(a.getMin(), -10);
    EXPECT_EQ(a.getMax(), 10);
    EXPECT_EQ(a.getTrueModifier(), -7);
    EXPECT_EQ(a.getEffectiveModifier(), -5);
}


TEST(Attribute, ModsButStillInBounds) {
    Attribute a(-10, 5, 10);
    a.nerf(3);

    EXPECT_EQ(a.get(), 2);
    EXPECT_EQ(a.getValue(), 5);
    EXPECT_EQ(a.getMin(), -10);
    EXPECT_EQ(a.getMax(), 10);
    EXPECT_EQ(a.getTrueModifier(), -3);
    EXPECT_EQ(a.getEffectiveModifier(), -3);
}


TEST(Attribute, CombiningBuffsAndNerfs) {
    Attribute a(-10, 0, 10);
    a.buff(5);
    a.nerf(2);
    a.buff(8);
    a.nerf(5);

    EXPECT_EQ(a.get(), 6);
    EXPECT_EQ(a.getValue(), 0);
    EXPECT_EQ(a.getMin(), -10);
    EXPECT_EQ(a.getMax(), 10);
    EXPECT_EQ(a.getTrueModifier(), 6);
    EXPECT_EQ(a.getEffectiveModifier(), 6);
}


TEST(Attribute, Overflow) {
    constexpr Attribute::IntType buff = std::numeric_limits<Attribute::IntType>::max() - 30;
    Attribute a(0);
    a.buff(buff);
    try {
        a.buff(42);
        EXPECT_FALSE("unreachable");
    } catch (const std::overflow_error&) {}

    EXPECT_EQ(a.get(), buff);
    EXPECT_EQ(a.getValue(), 0);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), buff);
    EXPECT_EQ(a.getEffectiveModifier(), buff);
}


TEST(Attribute, Underflow) {
    constexpr Attribute::IntType nerf = -(std::numeric_limits<Attribute::IntType>::min() + 30);
    Attribute a(0);
    a.nerf(nerf);
    try {
        a.nerf(42);
        EXPECT_FALSE("unreachable");
    } catch (const std::underflow_error&) {}

    EXPECT_EQ(a.get(), -nerf);
    EXPECT_EQ(a.getValue(), 0);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), -nerf);
    EXPECT_EQ(a.getEffectiveModifier(), -nerf);
}


TEST(Attribute, ClearOverflowOverflow) {
    Attribute a(0, 50, 100);

    // There is an overflow in attribute a
    a.buff(100);
    EXPECT_EQ(a.get(), 100);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 100);
    EXPECT_EQ(a.getEffectiveModifier(), 50);

    // Removing that overflow doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 100);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 50);
    EXPECT_EQ(a.getEffectiveModifier(), 50);

    // However, the buff is only 50!
    a.nerf(1);
    EXPECT_EQ(a.get(), 99);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 49);
    EXPECT_EQ(a.getEffectiveModifier(), 49);
}


TEST(Attribute, ClearOverflowUnderflow) {
    Attribute a(0, 50, 100);

    // There is an underflow in attribute a
    a.nerf(100);
    EXPECT_EQ(a.get(), 0);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), -100);
    EXPECT_EQ(a.getEffectiveModifier(), -50);

    // Removing that underflow doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 0);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), -50);
    EXPECT_EQ(a.getEffectiveModifier(), -50);

    // However, the nerf is only 50!
    a.buff(1);
    EXPECT_EQ(a.get(), 1);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), -49);
    EXPECT_EQ(a.getEffectiveModifier(), -49);
}


TEST(Attribute, ClearOverflowNoOverflowBuff) {
    Attribute a(0, 50, 100);

    // There isn't any overflow in attribute a
    a.buff(10);
    EXPECT_EQ(a.get(), 60);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 10);
    EXPECT_EQ(a.getEffectiveModifier(), 10);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 60);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 10);
    EXPECT_EQ(a.getEffectiveModifier(), 10);
}


TEST(Attribute, ClearOverflowNoOverflowNerf) {
    Attribute a(0, 50, 100);

    // There isn't any overflow in attribute a
    a.nerf(10);
    EXPECT_EQ(a.get(), 40);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), -10);
    EXPECT_EQ(a.getEffectiveModifier(), -10);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 40);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), -10);
    EXPECT_EQ(a.getEffectiveModifier(), -10);
}


TEST(Attribute, ClearOverflowNoModifier) {
    Attribute a(0, 50, 100);

    // There isn't any overflow in attribute a
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);
}


TEST(Attribute, ClearOverflowNoBoundsNoModifier) {
    Attribute a(50);

    // There isn't any overflow in attribute a
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);
}


TEST(Attribute, ClearOverflowNoBoundsNoUnderflowNerf) {
    Attribute a(50);
    a.nerf(5);

    // There isn't any overflow in attribute a
    EXPECT_EQ(a.get(), 45);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), -5);
    EXPECT_EQ(a.getEffectiveModifier(), -5);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 45);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), -5);
    EXPECT_EQ(a.getEffectiveModifier(), -5);
}


TEST(Attribute, ClearOverflowNoBoundsNoOverflowBuff) {
    Attribute a(50);
    a.buff(5);

    // There isn't any overflow in attribute a
    EXPECT_EQ(a.get(), 55);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), 5);
    EXPECT_EQ(a.getEffectiveModifier(), 5);

    // Removing eventual overflows doesn't change the current result
    a.clearOverflow();
    EXPECT_EQ(a.get(), 55);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), Attribute::noLowerBound);
    EXPECT_EQ(a.getMax(), Attribute::noUpperBound);
    EXPECT_EQ(a.getTrueModifier(), 5);
    EXPECT_EQ(a.getEffectiveModifier(), 5);
}


TEST(Attribute, Clear) {
    Attribute a(0, 50, 100);
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);

    // There is an overflow in attribute a
    a.buff(100);
    EXPECT_EQ(a.get(), 100);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 100);
    EXPECT_EQ(a.getEffectiveModifier(), 50);

    // Removing all the bonuses and maluses
    a.clear();
    EXPECT_EQ(a.get(), 50);
    EXPECT_EQ(a.getValue(), 50);
    EXPECT_EQ(a.getMin(), 0);
    EXPECT_EQ(a.getMax(), 100);
    EXPECT_EQ(a.getTrueModifier(), 0);
    EXPECT_EQ(a.getEffectiveModifier(), 0);
}


TEST(Character, Name) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    EXPECT_EQ(john.getName(), "John Doe");
}


TEST(Character, Team) {
    Character john("John Doe", 42, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    EXPECT_EQ(john.getTeam(), 42);
}


TEST(Character, Damage) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    EXPECT_EQ(john.getHealth().get(), 100);
    john.damage(10);
    EXPECT_EQ(john.getHealth().get(), 90);
}


TEST(Character, DamageUnderflow) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    EXPECT_EQ(john.getHealth().get(), 100);
    john.damage(1000);
    EXPECT_EQ(john.getHealth().get(), -101);
    john.heal(1);
    EXPECT_EQ(john.getHealth().get(), -100);
}


TEST(Character, Heal) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 50, 100));

    EXPECT_EQ(john.getHealth().get(), 50);
    john.heal(10);
    EXPECT_EQ(john.getHealth().get(), 60);
}


TEST(Character, HealOverflow) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 50, 100));

    EXPECT_EQ(john.getHealth().get(), 50);
    john.heal(1000);
    EXPECT_EQ(john.getHealth().get(), 100);
    john.damage(1);
    EXPECT_EQ(john.getHealth().get(), 99);
}


TEST(Character, CoherentHealth) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));
    auto& health = john.getHealth();
    health.nerf(10);

    EXPECT_EQ(health.get(), 90);
    EXPECT_EQ(john.getHealth().get(), health.get());
}


TEST(Character, CoherentActions) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));
    auto& actions = john.getActions();
    // Underflows, but we don't care
    actions.nerf(10);

    EXPECT_EQ(actions.get(), 0);
    EXPECT_EQ(john.getActions().get(), actions.get());
}


TEST(Character, BuiltinAttributesDistinct) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100), Attribute(0, 3, 3));
    EXPECT_NE(john.getHealth().get(), john.getActions().get());
}


TEST(Character, UserAttributeReservedName) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    try {
        john.addAttribute("__health", Attribute(0, 10, 10));
        EXPECT_FALSE("unreachable");
    } catch (const std::invalid_argument& err) {
        // The expected one!
    }
}


TEST(Character, UserAttributes) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    john.addAttribute("strength", Attribute(0, 20, 50));
    john.addAttribute("speed", Attribute(0, 30, 50));
    Attribute& strength = john.getAttribute("strength");
    Attribute& speed = john.getAttribute("speed");

    // Initial values
    EXPECT_EQ(strength.get(), 20);
    EXPECT_EQ(speed.get(), 30);

    // Altered values
    strength.buff(10);
    speed.nerf(40);
    EXPECT_EQ(strength.get(), 30);
    EXPECT_EQ(speed.get(), 0);

    // Aliasing
    EXPECT_EQ(strength.get(), john.getAttribute("strength").get());
    EXPECT_EQ(speed.get(), john.getAttribute("speed").get());
}


TEST(Character, BuiltinAttributes) {
    Character john("John Doe", 0, std::make_unique<heads::Greedy>(), Attribute(-101, 100, 100));

    // Initial values
    Attribute& health = john.getAttribute("__health");
    Attribute& actions = john.getAttribute("__actions");
    EXPECT_EQ(health.get(), 100);
    EXPECT_EQ(actions.get(), 1);

    // Altered values and aliasing
    john.getHealth().nerf(10);
    john.getActions().nerf(1);
    EXPECT_EQ(health.get(), 90);
    EXPECT_EQ(actions.get(), 0);
}


TEST(Simulation, Log) {
    Simulation s;
    s.log("Hello w", 0, "rld");

    EXPECT_EQ(s.getLogs(), "Hello w0rld\n");
}


TEST(Simulation, Random) {
    Simulation s;

    std::array<bool, 10> occured{false};
    for (int i = 0; i < 1000; ++i) {
        const auto res = s.random(1, 10);
        EXPECT_GE(res, 1);
        EXPECT_LE(res, 10);

        if (res > 0) {
            const size_t index = res - 1;
            if (index < occured.size()) {
                occured[index] = true;
            }
        }
    }

    for (size_t i = 0; i < occured.size(); ++i) {
        EXPECT_TRUE(occured[i]);
        if (!occured[i]) {
            std::cout << i + 1 << '\n';
        }
    }
}


TEST(Statistics, PositiveAverageAllPositive) {
    for (Attribute::IntType i = 1; i <= 1000; ++i) {
        for (Attribute::IntType j = 1; j <= i; ++j) {
            EXPECT_DOUBLE_EQ(positiveAverage(j, i), (i + j) / 2.);
        }
    }
}


TEST(Statistics, PositiveAverageNegativePart) {
    for (Attribute::IntType i = -1000; i <= 0; ++i) {
        for (Attribute::IntType j = 0; j <= 1000; ++j) {
            EXPECT_DOUBLE_EQ(positiveAverage(i, j), j * (j + 1) / (2. * (j - i + 1)));
        }
    }
}


TEST(Statistics, PositiveAverageMajoredNoMajoration) {
    for (Attribute::IntType i = -1000; i <= 0; ++i) {
        for (Attribute::IntType j = 0; j <= 1000; ++j) {
            EXPECT_DOUBLE_EQ(positiveAverageMajored(i, j, 1000), j * (j + 1) / (2. * (j - i + 1)));
        }
    }
}


TEST(Statistics, PositiveAverageMajoredTo500) {
    // Tests shall be simple enough, I won't write a whole formula in this.
    // I'll just check for a few properties
    double prev = 0;
    for (Attribute::IntType i = 1; i <= 1000; ++i) {
        // positiveAverageMajored(1, i, 500) -> 500 without reaching its limit
        const double current = positiveAverageMajored(1, i, 500);
        EXPECT_GT(current, prev);
        EXPECT_LT(current, 500);

        prev = current;
    }
}


TEST(Statistics, CompareRandomComputedByHand) {
    EXPECT_DOUBLE_EQ(compareRandom(1, 1), 0.);
    EXPECT_DOUBLE_EQ(compareRandom(1, 2), 0.5);
    EXPECT_DOUBLE_EQ(compareRandom(1, 3), 2. / 3.);
    EXPECT_DOUBLE_EQ(compareRandom(1, 4), 0.75);
    EXPECT_DOUBLE_EQ(compareRandom(1, 5), 0.8);
    EXPECT_DOUBLE_EQ(compareRandom(2, 1), 0.);
    EXPECT_DOUBLE_EQ(compareRandom(2, 2), 0.25);
    EXPECT_DOUBLE_EQ(compareRandom(2, 3), 0.5);
    EXPECT_DOUBLE_EQ(compareRandom(2, 4), 5. / 8.);
    EXPECT_DOUBLE_EQ(compareRandom(2, 5), 0.7);
    EXPECT_DOUBLE_EQ(compareRandom(3, 1), 0.);
    EXPECT_DOUBLE_EQ(compareRandom(3, 2), 1. / 6.);
    EXPECT_DOUBLE_EQ(compareRandom(3, 3), 1. / 3.);
    EXPECT_DOUBLE_EQ(compareRandom(3, 4), 0.5);
    EXPECT_DOUBLE_EQ(compareRandom(3, 5), 9. / 15.);
    EXPECT_DOUBLE_EQ(compareRandom(4, 1), 0.);
    EXPECT_DOUBLE_EQ(compareRandom(4, 2), 1. / 8.);
    EXPECT_DOUBLE_EQ(compareRandom(4, 3), 0.25);
    EXPECT_DOUBLE_EQ(compareRandom(4, 4), 6. / 16.);
    EXPECT_DOUBLE_EQ(compareRandom(4, 5), 0.5);
    EXPECT_DOUBLE_EQ(compareRandom(5, 1), 0.);
    EXPECT_DOUBLE_EQ(compareRandom(5, 2), 0.1);
    EXPECT_DOUBLE_EQ(compareRandom(5, 3), 0.2);
    EXPECT_DOUBLE_EQ(compareRandom(5, 4), 6. / 20.);
    EXPECT_DOUBLE_EQ(compareRandom(5, 5), 10. / 25.);
}


TEST(Statistics, CompareRandomFuzzy) {
    for (Attribute::IntType i = 1; i <= 1000; ++i) {
        for (Attribute::IntType j = 1; j <= 1000; ++j) {
            const auto res = compareRandom(i, j);
            EXPECT_GE(res, 0.);
            EXPECT_LE(res, 1.);
        }
    }
}


TEST(SkillEfficiency, Default) {
    Skill::Efficiency eff;
    EXPECT_DOUBLE_EQ(eff.total(), 0.);

    static_assert(static_cast<int>(Skill::Usage::Max) == 4);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], 0.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Defense], 0.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Buff], 0.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Nerf], 0.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Healing], 0.);

    const auto& ceff = eff;
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Attack], 0.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Defense], 0.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Buff], 0.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Nerf], 0.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Healing], 0.);

    EXPECT_DOUBLE_EQ(ceff.getSuccessRate(), 0.);
}


TEST(SkillEfficiency, AllToOne) {
    Skill::Efficiency eff;

    static_assert(static_cast<int>(Skill::Usage::Max) == 4);
    eff[Skill::Usage::Attack] = 1.;
    eff[Skill::Usage::Defense] = 1.;
    eff[Skill::Usage::Buff] = 1.;
    eff[Skill::Usage::Nerf] = 1.;
    eff[Skill::Usage::Healing] = 1.;
    eff.setSuccessRate(1.);

    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], 1.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Defense], 1.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Buff], 1.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Nerf], 1.);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Healing], 1.);

    const auto& ceff = eff;
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Attack], 1.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Defense], 1.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Buff], 1.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Nerf], 1.);
    EXPECT_DOUBLE_EQ(ceff[Skill::Usage::Healing], 1.);

    EXPECT_DOUBLE_EQ(eff.total(), 5.);

    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
}


TEST(SkillEfficiency, AllUnique) {
    Skill::Efficiency eff;
    eff.setSuccessRate(.5);

    for (size_t i = 0; i <= static_cast<size_t>(Skill::Usage::Max); ++i) {
        eff[static_cast<Skill::Usage>(i)] = i;
    }

    for (size_t i = 0; i <= static_cast<size_t>(Skill::Usage::Max); ++i) {
        EXPECT_DOUBLE_EQ(eff[static_cast<Skill::Usage>(i)], static_cast<double>(i));
    }

    const auto max = static_cast<uint_fast32_t>(Skill::Usage::Max);
    EXPECT_DOUBLE_EQ(eff.total(), max * (max + 1) / 2);

    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), .5);
}


TEST(SingleAttack, NormalCase) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1000, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    // Cheems always succeeds and Horny always accepts the hit
    cheems.addAttribute(attack, Attribute(1, 100, 100));
    cheems.addAttribute(randDmg, Attribute(21));
    cheems.addAttribute(fixedDmg, Attribute(49));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg));
    auto& bonk = *cheems.getSkills().begin();

    for (size_t i = 0; i < 1000; ++i) {
        Character horny("Horny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
        EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
        EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(bonk.isAvailable(cheems));
        constexpr double expected = 60.;
        const auto eff = bonk.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], expected);
        EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
        EXPECT_DOUBLE_EQ(eff.total(), expected);

        // Go to horny jail!
        bonk.use(simulation, cheems, horny);
        const auto health = horny.getHealth().get();
        EXPECT_LE(health, 50);
        EXPECT_GE(health, 30);
    }

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


TEST(SingleAttack, Malus) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1000, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    // Cheems always succeeds and Horny always accepts the hit
    cheems.addAttribute(attack, Attribute(1, 100, 100));
    cheems.addAttribute(randDmg, Attribute(5));
    cheems.addAttribute(fixedDmg, Attribute(0));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg));
    auto& bonk = *cheems.getSkills().begin();
    // Adding a malus
    cheems.getAttribute(fixedDmg).nerf(3);

    for (size_t i = 0; i < 1000; ++i) {
        Character horny("Horny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
        EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
        EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(bonk.isAvailable(cheems));
        constexpr double expected = 3. / 5.;
        const auto eff = bonk.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], expected);
        EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
        EXPECT_DOUBLE_EQ(eff.total(), expected);

        // Go to horny jail!
        bonk.use(simulation, cheems, horny);
        const auto health = horny.getHealth().get();
        EXPECT_LE(health, 100);
        EXPECT_GE(health, 98);
    }

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


TEST(SingleAttack, Bonus) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1000, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    // Cheems always succeeds and Horny always accepts the hit
    cheems.addAttribute(attack, Attribute(1, 100, 100));
    cheems.addAttribute(randDmg, Attribute(5));
    cheems.addAttribute(fixedDmg, Attribute(0));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg));
    auto& bonk = *cheems.getSkills().begin();
    // Adding a buff
    cheems.getAttribute(fixedDmg).buff(3);

    for (size_t i = 0; i < 1000; ++i) {
        Character horny("Horny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
        EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
        EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(bonk.isAvailable(cheems));
        constexpr double expected = 6.;
        const auto eff = bonk.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], expected);
        EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
        EXPECT_DOUBLE_EQ(eff.total(), expected);

        // Go to horny jail!
        bonk.use(simulation, cheems, horny);
        const auto health = horny.getHealth().get();
        EXPECT_LE(health, 96);
        EXPECT_GE(health, 92);
    }

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


TEST(SingleAttack, Lethal) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1000, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    // Cheems always succeeds and Horny always accepts the hit
    cheems.addAttribute(attack, Attribute(1, 100, 100));
    cheems.addAttribute(randDmg, Attribute(51));
    cheems.addAttribute(fixedDmg, Attribute(99));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg));
    auto& bonk = *cheems.getSkills().begin();

    for (size_t i = 0; i < 1000; ++i) {
        Character horny("Horny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
        EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
        EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(bonk.isAvailable(cheems));
        // Twice the efficiency because horny dies in the median case
        constexpr double expected = 250.;
        const auto eff = bonk.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], expected);
        EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
        EXPECT_DOUBLE_EQ(eff.total(), expected);

        // Go to horny jail!
        bonk.use(simulation, cheems, horny);
        const auto health = horny.getHealth().get();
        EXPECT_EQ(health, 0);
    }

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


class Dodge: public Head {
public:
    void onMyTurn(Simulation&, Character&) override {
        throw std::logic_error("Not implemented");
    }

    double chancesToHit(
        const Character&,
        Attribute::IntType,
        Attribute::IntType,
        Attribute::IntType
    ) const override {
        // Indeed, it has no chance to make any damage
        return 0.;
    }

    Head::Decision onAttack(Simulation&, Character&, Character&, Attribute::IntType, Attribute::IntType, Attribute::IntType) override {
        return Decision::Deny;
    }
};

TEST(SingleAttack, Dodge) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    // Cheems always succeeds but Horny never accepts the hit
    cheems.addAttribute(attack, Attribute(1, 100, 100));
    cheems.addAttribute(randDmg, Attribute(51));
    cheems.addAttribute(fixedDmg, Attribute(99));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg));
    auto& bonk = *cheems.getSkills().begin();

    Character horny("Horny", 2, std::make_unique<Dodge>(), Attribute(0, 100, 100));
    EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
    EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
    EXPECT_TRUE(bonk.isAvailable(cheems));
    // Twice the efficiency because horny dies in the median case
    constexpr double expected = 250.;
    const auto eff = bonk.efficiency(cheems, horny);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Attack], expected);
    // However, he can't get hit
    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 0.);
    EXPECT_DOUBLE_EQ(eff.total(), expected);

    // Go to horny jail!
    bonk.use(simulation, cheems, horny);
    const auto health = horny.getHealth().get();
    // Horny dodged
    EXPECT_EQ(health, 100);

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


TEST(SingleAttack, Modifiers) {
    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1000, 1000));
    const std::string attack("attack");
    const std::string randDmg("randDmg");
    const std::string fixedDmg("fixedDmg");
    const std::string physical("physical");
    const std::string magical("magical");
    cheems.addAttribute(attack, Attribute(1, 95, 100));
    cheems.addAttribute(randDmg, Attribute(21));
    cheems.addAttribute(fixedDmg, Attribute(49));
    cheems.addAttribute(physical, Attribute(0));
    cheems.addAttribute(magical, Attribute(0));

    auto bonkPtr = std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg);
    bonkPtr->addModifier(physical);
    cheems.addSkill(std::move(bonkPtr));

    auto magicPtr = std::make_unique<skills::SingleAttack>("magic", skills::RollMode::OneToOneHundred, attack, randDmg, fixedDmg);
    magicPtr->addModifier(magical);
    cheems.addSkill(std::move(magicPtr));

    auto it = cheems.getSkills().begin();
    auto& bonk = *it;
    ++it;
    auto& magic = *it;
    ++it;
    EXPECT_EQ(it, cheems.getSkills().end());

    cheems.getAttribute(physical).buff(5);
    cheems.getAttribute(magical).nerf(5);

    for (size_t i = 0; i < 1000; ++i) {
        Character horny("Horny", 2, std::make_unique<heads::Greedy>(0, std::string(), skills::RollMode::OneToOneHundred), Attribute(0, 100, 100));

        // These skills use the same main attribute, but have different
        // modifiers. Here, cheems has a bonus for its physical attacks and a
        // malus for its magical attacks, so we have a higher success rate for
        // bonk than magic
        EXPECT_EQ(bonk.usage(), Skill::Usage::Attack);
        EXPECT_EQ(bonk.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(bonk.isAvailable(cheems));
        constexpr double bExpected = 60.;
        const auto bEff = bonk.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(bEff[Skill::Usage::Attack], bExpected);
        EXPECT_DOUBLE_EQ(bEff.getSuccessRate(), 1.);
        EXPECT_DOUBLE_EQ(bEff.total(), bExpected);

        EXPECT_EQ(magic.usage(), Skill::Usage::Attack);
        EXPECT_EQ(magic.target(), Skill::TargetType::Individual);
        EXPECT_TRUE(magic.isAvailable(cheems));
        constexpr double mExpected = 60.;
        const auto mEff = magic.efficiency(cheems, horny);
        EXPECT_DOUBLE_EQ(mEff[Skill::Usage::Attack], mExpected);
        EXPECT_DOUBLE_EQ(mEff.getSuccessRate(), 0.9);
        EXPECT_DOUBLE_EQ(mEff.total(), mExpected);

        // Go to horny jail!
        bonk.use(simulation, cheems, horny);
        const auto health = horny.getHealth().get();
        EXPECT_LE(health, 50);
        EXPECT_GE(health, 30);
    }

    EXPECT_FALSE(bonk.isAvailable(cheems));
}


TEST(SingleHeal, SelfHeal) {
    Simulation simulation;
    const std::string randHeal("randHeal");
    const std::string fixedHeal("fixedHeal");

    Character foo("Foo", 1, std::make_unique<heads::Greedy>(), Attribute(0, 50, 50), Attribute(0, 2, 2));

    foo.addAttribute(randHeal, Attribute(0, 5, Attribute::noUpperBound));
    foo.addAttribute(fixedHeal, Attribute(0, 0, Attribute::noLowerBound));
    foo.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToAttr, std::string(), randHeal, fixedHeal, false));

    foo.damage(10);
    EXPECT_EQ(foo.getHealth().get(), 40);

    auto& heal = *foo.getSkills().begin();
    EXPECT_EQ(heal.usage(), Skill::Usage::Healing);
    EXPECT_EQ(heal.target(), Skill::TargetType::User);
    EXPECT_TRUE(heal.isAvailable(foo));
    constexpr double expected = 3.;
    const auto eff = heal.efficiency(foo);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Healing], expected);
    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
    EXPECT_DOUBLE_EQ(eff.total(), expected);

    heal.use(simulation, foo);

    EXPECT_LE(foo.getHealth().get(), 45);
    EXPECT_GE(foo.getHealth().get(), 41);
}


TEST(SingleHeal, SelfHealWithUnderflow) {
    Simulation simulation;
    const std::string randHeal("randHeal");
    const std::string fixedHeal("fixedHeal");
    const std::string medicine("medicine");

    Character foo("Foo", 1, std::make_unique<heads::Greedy>(), Attribute(0, 50, 50), Attribute(0, 1, 1));

    foo.addAttribute(randHeal, Attribute(Attribute::noLowerBound, 5, Attribute::noUpperBound));
    foo.addAttribute(fixedHeal, Attribute(Attribute::noLowerBound, 0, Attribute::noLowerBound));
    foo.addAttribute(medicine, Attribute(5, 60, 95));
    foo.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToOneHundred, medicine, randHeal, fixedHeal, false));

    foo.getAttribute(fixedHeal).nerf(3);
    foo.damage(10);
    EXPECT_EQ(foo.getHealth().get(), 40);

    auto& heal = *foo.getSkills().begin();
    EXPECT_EQ(heal.usage(), Skill::Usage::Healing);
    EXPECT_EQ(heal.target(), Skill::TargetType::User);
    EXPECT_TRUE(heal.isAvailable(foo));
    constexpr double expected = 3. / 5.;
    const auto eff = heal.efficiency(foo);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Healing], expected);
    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 0.6);
    EXPECT_DOUBLE_EQ(eff.total(), expected);

    heal.use(simulation, foo);

    EXPECT_LE(foo.getHealth().get(), 45);
    EXPECT_GE(foo.getHealth().get(), 40);
    EXPECT_EQ(foo.getActions().get(), 0);
}


TEST(SingleHeal, SelfHealWithOverflow) {
    Simulation simulation;
    const std::string randHeal("randHeal");
    const std::string fixedHeal("fixedHeal");

    Character foo("Foo", 1, std::make_unique<heads::Greedy>(), Attribute(0, 50, 50), Attribute(0, 1, 1));

    foo.addAttribute(randHeal, Attribute(Attribute::noLowerBound, 5, Attribute::noUpperBound));
    foo.addAttribute(fixedHeal, Attribute(Attribute::noLowerBound, 0, Attribute::noLowerBound));
    foo.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToAttr, std::string(), randHeal, fixedHeal, false));

    foo.getAttribute(fixedHeal).buff(8);
    foo.damage(10);
    EXPECT_EQ(foo.getHealth().get(), 40);

    auto& heal = *foo.getSkills().begin();
    EXPECT_EQ(heal.usage(), Skill::Usage::Healing);
    EXPECT_EQ(heal.target(), Skill::TargetType::User);
    EXPECT_TRUE(heal.isAvailable(foo));
    constexpr double expected = 49. / 5;
    const auto eff = heal.efficiency(foo);
    EXPECT_DOUBLE_EQ(eff[Skill::Usage::Healing], expected);
    EXPECT_DOUBLE_EQ(eff.getSuccessRate(), 1.);
    EXPECT_DOUBLE_EQ(eff.total(), expected);

    heal.use(simulation, foo);

    EXPECT_LE(foo.getHealth().get(), 50);
    EXPECT_GE(foo.getHealth().get(), 49);
}


TEST(SingleHeal, TwoTargetsWithGreedyHead) {
    Simulation simulation;
    const std::string randHeal("randHeal");
    const std::string fixedHeal("fixedHeal");

    Character healer("healer", 1, std::make_unique<heads::Greedy>(), Attribute(0, 50, 50), Attribute(0, 1, 1));
    healer.damage(1);
    healer.addAttribute(randHeal, Attribute(0, 5, Attribute::noUpperBound));
    healer.addAttribute(fixedHeal, Attribute(0, 0, Attribute::noUpperBound));
    healer.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToAttr, std::string(), randHeal, fixedHeal, true));
    simulation.addCharacter(std::move(healer));

    Character other("other", 1, std::make_unique<heads::Greedy>(), Attribute(0, 50, 50), Attribute(0, 1, 1));
    other.damage(5);
    simulation.addCharacter(std::move(other));

    Character& healerRef = *simulation.getCharacters().begin();
    Head& healerHead = healerRef.getHead();
    healerHead.onNewTurn(simulation, healerRef);
    healerHead.onMyTurn(simulation, healerRef);

    EXPECT_EQ(healerRef.getHealth().get(), 49);
    EXPECT_GE((simulation.getCharacters().begin() + 1)->getHealth().get(), 46);
}


TEST(Head, Greedy) {
    constexpr auto accuracy = "accuracy";
    constexpr auto minDmg = "minDmg";
    constexpr auto maxDmg = "maxDmg";

    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 5, 5));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addAttribute(minDmg, Attribute(0, 5, 10));
    cheems.addAttribute(maxDmg, Attribute(0, 10, 50));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addSkill(std::make_unique<skills::SingleAttack>(
        "bonk",
        skills::RollMode::OneToAttr,
        accuracy,
        minDmg,
        maxDmg
    ));

    simulation.addCharacter(std::move(cheems));

    constexpr auto evasion = "evasion";
    std::array names {"Horny", "Doge", "uwu", "owo"};
    for (const auto& i: names) {
        Character target(i, 2, std::make_unique<heads::Greedy>(1, evasion, heads::RollMode::OneToAttr), Attribute(0, 10, 10));
        target.addAttribute(evasion, Attribute(1, 10, 10));
        simulation.addCharacter(std::move(target));
    }

    size_t nbAlive = names.size() + 1;
    size_t nbTurns = 0;
    constexpr size_t maxTurns = 100;
    while (nbAlive > 1 && nbTurns < maxTurns) {
        for (auto& i: simulation.getCharacters()) {
            i.getActions().clear();
            i.getHead().onNewTurn(simulation, i);
        }

        for (auto& i: simulation.getCharacters()) {
            if (i.getHealth().get()) {
                i.getHead().onMyTurn(simulation, i);
            }
        }

        nbAlive = 0;
        for (const auto& i: simulation.getCharacters()) {
            if (i.getHealth().get()) {
                ++nbAlive;
            }
        }

        ++nbTurns;
    }

    EXPECT_LT(nbTurns, maxTurns);
    if (nbTurns == maxTurns) {
        std::cerr << simulation.getLogs();
    }
}


TEST(Head, GreedyWithDodge) {
    Simulation s;

    constexpr auto strength = "strength";
    constexpr auto minDmg = "minDmg";
    constexpr auto maxDmg = "maxDmg";
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(4, strength, heads::RollMode::OneToAttr), Attribute(0, 100, 100), Attribute(0, 5, 5));
    cheems.addAttribute(strength, Attribute(1));
    cheems.addAttribute(minDmg, Attribute(10));
    cheems.addAttribute(maxDmg, Attribute(10));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
    s.addCharacter(std::move(cheems));

    Character horny("Horny", 2, std::make_unique<heads::Greedy>(1, strength, heads::RollMode::OneToAttr), Attribute(0, 10, 10), Attribute(0, 2, 2));
    horny.addAttribute(strength, Attribute(std::numeric_limits<Attribute::IntType>::max()));
    horny.addAttribute(minDmg, Attribute(100));
    horny.addAttribute(maxDmg, Attribute(100));
    horny.addSkill(std::make_unique<skills::SingleAttack>("counter-bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
    s.addCharacter(std::move(horny));

    const auto iCheems = s.getCharacters().begin();
    const auto iHorny = iCheems + 1;
    iCheems->getHead().onNewTurn(s, *iCheems);
    iHorny->getHead().onNewTurn(s, *iHorny);

    // Cheems attacks Horny. Horny tries to dodge
    iCheems->getHead().onMyTurn(s, *iCheems);
    EXPECT_EQ(iCheems->getActions().get(), 4);
    EXPECT_EQ(iHorny->getActions().get(), 1);

    // It is possible but very unlikely that Cheems hits Horny
    ASSERT_EQ(iHorny->getHealth().get(), 10);

    // Now, time for Horny's revenge
    EXPECT_GE(iHorny->getActions().get(), 1);
    iHorny->getHead().onMyTurn(s, *iHorny);

    // It is possible but very unlikely that Cheems dodges
    EXPECT_EQ(iCheems->getHealth().get(), 0);
}


TEST(Head, GreedySuccessRate) {
    Simulation s;

    constexpr auto strength = "strength";
    constexpr auto bigBonkMin = "bigBonkMin";
    constexpr auto bigBonkMax = "bigBonkMax";
    constexpr auto speed = "speed";
    constexpr auto fastBonkMin = "fastBonkMin";
    constexpr auto fastBonkMax = "fastBonkMax";

    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(0, std::string(), skills::RollMode::OneToOneHundred), Attribute(0, 100, 100));
    // 10% of chances to deal 2-5 = 0.1 * (2 + 5) / 2 = 0.35
    cheems.addAttribute(strength, Attribute(5, 10, 95));
    cheems.addAttribute(bigBonkMin, Attribute(0, 2, Attribute::noUpperBound));
    cheems.addAttribute(bigBonkMax, Attribute(0, 5, Attribute::noUpperBound));
    // 80% of chances to deal 1-4 = 0.8 * (1 + 4) / 2 = 2
    cheems.addAttribute(speed, Attribute(5, 80, 95));
    cheems.addAttribute(fastBonkMin, Attribute(0, 1, Attribute::noUpperBound));
    cheems.addAttribute(fastBonkMax, Attribute(0, 4, Attribute::noUpperBound));

    cheems.addSkill(std::make_unique<skills::SingleAttack>("big_bonk", skills::RollMode::OneToOneHundred, strength, bigBonkMin, bigBonkMax));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("fast_bonk", skills::RollMode::OneToOneHundred, speed, fastBonkMin, fastBonkMax));
    s.addCharacter(std::move(cheems));

    Character horny("horny", 2, std::make_unique<heads::Greedy>(0, std::string(), skills::RollMode::OneToOneHundred), Attribute(0, 100, 100));
    s.addCharacter(std::move(horny));

    auto chars = s.getCharacters().begin();
    chars->getHead().onMyTurn(s, *chars);

    // We check in the logs the skill that cheems used to beat horny to death
    const auto logs = s.getLogs();
    EXPECT_EQ(strstr(logs.c_str(), "big_bonk"), nullptr);
    EXPECT_NE(strstr(logs.c_str(), "fast_bonk"), nullptr);
}


TEST(Head, GreedyGreaterDamage) {
    Simulation s;

    constexpr auto strength = "strength";
    constexpr auto bigBonkMin = "bigBonkMin";
    constexpr auto bigBonkMax = "bigBonkMax";
    constexpr auto speed = "speed";
    constexpr auto fastBonkMin = "fastBonkMin";
    constexpr auto fastBonkMax = "fastBonkMax";

    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
    // 10% of chances to deal 10-15 = 0.1 * (10 + 15) / 2 = 2.5
    cheems.addAttribute(strength, Attribute(5, 10, 95));
    cheems.addAttribute(bigBonkMin, Attribute(0, 2, Attribute::noUpperBound));
    cheems.addAttribute(bigBonkMax, Attribute(0, 5, Attribute::noUpperBound));
    // 80% of chances to deal 1-4 = 0.8 * (1 + 4) / 2 = 2
    cheems.addAttribute(speed, Attribute(5, 80, 95));
    cheems.addAttribute(fastBonkMin, Attribute(0, 1, Attribute::noUpperBound));
    cheems.addAttribute(fastBonkMax, Attribute(0, 4, Attribute::noUpperBound));

    cheems.addSkill(std::make_unique<skills::SingleAttack>("big_bonk", skills::RollMode::OneToOneHundred, strength, bigBonkMin, bigBonkMax));
    cheems.addSkill(std::make_unique<skills::SingleAttack>("fast_bonk", skills::RollMode::OneToOneHundred, speed, fastBonkMin, fastBonkMax));
    s.addCharacter(std::move(cheems));

    Character horny("horny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100));
    s.addCharacter(std::move(horny));

    auto chars = s.getCharacters().begin();
    chars->getHead().onMyTurn(s, *chars);

    // We check in the logs the skill that cheems used to beat horny to death
    const auto logs = s.getLogs();
    EXPECT_NE(strstr(logs.c_str(), "big_bonk"), nullptr);
    EXPECT_EQ(strstr(logs.c_str(), "fast_bonk"), nullptr);
}


TEST(Simulation, Run) {
    constexpr auto accuracy = "accuracy";
    constexpr auto minDmg = "minDmg";
    constexpr auto maxDmg = "maxDmg";

    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 5, 5));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addAttribute(minDmg, Attribute(0, 5, 10));
    cheems.addAttribute(maxDmg, Attribute(0, 10, 50));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addSkill(std::make_unique<skills::SingleAttack>(
        "bonk",
        skills::RollMode::OneToAttr,
        accuracy,
        minDmg,
        maxDmg
    ));

    simulation.addCharacter(std::move(cheems));

    constexpr auto evasion = "evasion";
    std::array names {"Horny", "Doge", "uwu", "owo"};
    for (const char* i: names) {
        Character target(i, 2, std::make_unique<heads::Greedy>(1, evasion, heads::RollMode::OneToAttr), Attribute(0, 10, 10));
        target.addAttribute(evasion, Attribute(1, 10, 10));
        simulation.addCharacter(std::move(target));
    }

    // We hope this terminates
    simulation.run();

    for (const auto& i: simulation.getCharacters()) {
        switch (i.getTeam()) {
        case 1:
            EXPECT_EQ(i.getHealth().get(), 100);
            break;
        default:
            EXPECT_EQ(i.getHealth().get(), 0);
            break;
        }
    }
}


TEST(Simulation, Winner) {
    constexpr auto accuracy = "accuracy";
    constexpr auto minDmg = "minDmg";
    constexpr auto maxDmg = "maxDmg";

    Simulation simulation;
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 5, 5));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addAttribute(minDmg, Attribute(0, 5, 10));
    cheems.addAttribute(maxDmg, Attribute(0, 10, 50));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addSkill(std::make_unique<skills::SingleAttack>(
        "bonk",
        skills::RollMode::OneToAttr,
        accuracy,
        minDmg,
        maxDmg
    ));

    simulation.addCharacter(std::move(cheems));

    constexpr auto evasion = "evasion";
    std::array names {"Horny", "Doge", "uwu", "owo"};
    for (const char* i: names) {
        Character target(i, 2, std::make_unique<heads::Greedy>(1, evasion, heads::RollMode::OneToAttr), Attribute(0, 10, 10));
        target.addAttribute(evasion, Attribute(1, 10, 10));
        simulation.addCharacter(std::move(target));
    }

    // We hope this terminates
    simulation.run();

    EXPECT_EQ(simulation.winner(), 1);
}


TEST(Simulation, WinnerAtTheEnd) {
    constexpr auto accuracy = "accuracy";
    constexpr auto minDmg = "minDmg";
    constexpr auto maxDmg = "maxDmg";

    constexpr auto evasion = "evasion";
    Simulation simulation;
    std::array names {"Horny", "Doge", "uwu", "owo"};
    for (const char* i: names) {
        Character target(i, 2, std::make_unique<heads::Greedy>(1, evasion), Attribute(0, 10, 10));
        target.addAttribute(evasion, Attribute(1, 10, 10));
        simulation.addCharacter(std::move(target));
    }

    // This time, we insert Cheems at the end
    Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 5, 5));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addAttribute(minDmg, Attribute(0, 5, 10));
    cheems.addAttribute(maxDmg, Attribute(0, 10, 50));
    cheems.addAttribute(accuracy, Attribute(2, 20, 20));
    cheems.addSkill(std::make_unique<skills::SingleAttack>(
        "bonk",
        skills::RollMode::OneToAttr,
        accuracy,
        minDmg,
        maxDmg
    ));

    simulation.addCharacter(std::move(cheems));

    // We hope this terminates
    simulation.run();

    EXPECT_EQ(simulation.winner(), 1);
}


TEST(Simulation, Interruption) {
    ThreadedRuntime rt(1);

    rt.run([]() {
        Simulation s;
        s.addCharacter(Character("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1, Attribute::noUpperBound)));
        s.addCharacter(Character("NotHorny", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 1, Attribute::noUpperBound)));
        return s;
    }, 1, 1);

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    rt.requestInterruption();

    // If this test terminates, it's ok
    rt.wait();
}


TEST(Runtime, Basic) {
    ThreadedRuntime rt(1);
    rt.run([]() -> Simulation {
        constexpr auto strength = "strength";
        constexpr auto minDmg = "minDmg";
        constexpr auto maxDmg = "maxDmg";
        Simulation s;
        Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 4, 4));
        cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
        cheems.addAttribute(strength, Attribute(2, 50, 100));
        cheems.addAttribute(minDmg, Attribute(0, 5, 100));
        cheems.addAttribute(maxDmg, Attribute(0, 20, 100));
        s.addCharacter(std::move(cheems));

        const std::array names{"Horny", "Doge", "uwu", "owo"};
        for (const char* i: names) {
            Character npc(i, 2, std::make_unique<heads::Greedy>(), Attribute(0, 20, 20), Attribute(0, 1, 1));
            npc.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
            npc.addAttribute(strength, Attribute(2, 20, 100));
            npc.addAttribute(minDmg, Attribute(0, 2, 100));
            npc.addAttribute(maxDmg, Attribute(0, 10, 100));
            s.addCharacter(std::move(npc));
        }

        return s;
    }, 8, 1000);
    rt.wait();

    EXPECT_DOUBLE_EQ(rt.percent(), 100.0);
    EXPECT_EQ(rt.completed(), 1000);
}


TEST(Runtime, ThreadCountAutodetection) {
    ThreadedRuntime rt(1);
    rt.run([]() -> Simulation {
        constexpr auto strength = "strength";
        constexpr auto minDmg = "minDmg";
        constexpr auto maxDmg = "maxDmg";
        Simulation s;
        Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 4, 4));
        cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
        cheems.addAttribute(strength, Attribute(2, 50, 100));
        cheems.addAttribute(minDmg, Attribute(0, 5, 100));
        cheems.addAttribute(maxDmg, Attribute(0, 20, 100));
        s.addCharacter(std::move(cheems));

        const std::array names{"Horny", "Doge", "uwu", "owo"};
        for (const char* i: names) {
            Character npc(i, 2, std::make_unique<heads::Greedy>(), Attribute(0, 20, 20), Attribute(0, 1, 1));
            npc.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
            npc.addAttribute(strength, Attribute(2, 20, 100));
            npc.addAttribute(minDmg, Attribute(0, 2, 100));
            npc.addAttribute(maxDmg, Attribute(0, 10, 100));
            s.addCharacter(std::move(npc));
        }

        return s;
    }, 0, 1000);
    rt.wait();

    EXPECT_DOUBLE_EQ(rt.percent(), 100.0);
    EXPECT_EQ(rt.completed(), 1000);
}


TEST(Runtime, Frequencies) {
    ThreadedRuntime rt(1);
    rt.run([]() -> Simulation {
        constexpr auto strength = "strength";
        constexpr auto minDmg = "minDmg";
        constexpr auto maxDmg = "maxDmg";
        Simulation s;
        Character cheems("Cheems", 1, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 4, 4));
        cheems.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
        cheems.addAttribute(strength, Attribute(2, 50, 100));
        cheems.addAttribute(minDmg, Attribute(0, 5, 100));
        cheems.addAttribute(maxDmg, Attribute(0, 20, 100));
        s.addCharacter(std::move(cheems));

        Character horny("Cheems", 2, std::make_unique<heads::Greedy>(), Attribute(0, 100, 100), Attribute(0, 4, 4));
        horny.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, strength, minDmg, maxDmg));
        horny.addAttribute(strength, Attribute(2, 50, 100));
        horny.addAttribute(minDmg, Attribute(0, 5, 100));
        horny.addAttribute(maxDmg, Attribute(0, 20, 100));
        s.addCharacter(std::move(horny));

        return s;
    }, 8, 1000);
    rt.wait();

    EXPECT_DOUBLE_EQ(rt.percent(), 100.0);
    EXPECT_EQ(rt.completed(), 1000);

    // Strictly impossible
    EXPECT_LE(rt.victoryRate(), 1.);
    EXPECT_GE(rt.victoryRate(), 0.);

    // Unlikely
    EXPECT_LT(rt.victoryRate(), 1.);
    EXPECT_GT(rt.victoryRate(), 0.);
}


TEST(RealWorld, InfiniteLoopError) {
    ThreadedRuntime rt(1);
    rt.run([]() {
#define NEW_ATTR(name) constexpr auto name = #name
        NEW_ATTR(bonkPow);
        NEW_ATTR(bonkRand);
        NEW_ATTR(bonkFixed);
        NEW_ATTR(healPow);
        NEW_ATTR(healRand);
        NEW_ATTR(healFixed);
        NEW_ATTR(cleavePow);
        NEW_ATTR(cleaveRand);
        NEW_ATTR(cleaveFixed);
        NEW_ATTR(evasion);
#undef NEW_ATTR

        Simulation s;

        Character dps("DPS", 1, std::make_unique<heads::Greedy>(1, evasion), Attribute(0, 10, 10), Attribute(0, 2, Attribute::noUpperBound));
        dps.addAttribute(bonkPow, Attribute(1, 10, Attribute::noUpperBound));
        dps.addAttribute(bonkRand, Attribute(0, 5, Attribute::noUpperBound));
        dps.addAttribute(bonkFixed, Attribute(0, 1, Attribute::noUpperBound));
        dps.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        dps.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, bonkPow, bonkRand, bonkFixed));

        Character healer("healer", 1, std::make_unique<heads::Greedy>(1, evasion), Attribute(0, 10, 10), Attribute(0, 2, Attribute::noUpperBound));
        healer.addAttribute(healPow, Attribute(1, 10, Attribute::noUpperBound));
        healer.addAttribute(healRand, Attribute(0, 5, Attribute::noUpperBound));
        healer.addAttribute(healFixed, Attribute(0, 1, Attribute::noUpperBound));
        healer.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        healer.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToAttr, healPow, healRand, healFixed, true));

        Character boss("boss", 2, std::make_unique<heads::Greedy>(2, evasion), Attribute(0, 50, 50), Attribute(0, 4, Attribute::noUpperBound));
        boss.addAttribute(cleavePow, Attribute(1, 10, Attribute::noUpperBound));
        boss.addAttribute(cleaveRand, Attribute(0, 3, Attribute::noUpperBound));
        boss.addAttribute(cleaveFixed, Attribute(0, 1, Attribute::noUpperBound));
        boss.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        boss.addSkill(std::make_unique<skills::SingleAttack>("cleave", skills::RollMode::OneToAttr, cleavePow, cleaveRand, cleaveFixed));

        s.addCharacter(std::move(dps));
        s.addCharacter(std::move(healer));
        s.addCharacter(std::move(boss));
        return s;
    }, 0, 1000);
    rt.wait();

    // If this terminate, it's ok
    std::cout << "victory rate: " << rt.victoryRate() << '\n';
}


TEST(Runtime, Interruptions) {
    ThreadedRuntime rt(1);
    rt.run([]() {
#define NEW_ATTR(name) constexpr auto name = #name
        NEW_ATTR(bonk_pow);
        NEW_ATTR(bonk_min);
        NEW_ATTR(bonk_max);
        NEW_ATTR(heal_pow);
        NEW_ATTR(heal_min);
        NEW_ATTR(heal_max);
        NEW_ATTR(cleave_pow);
        NEW_ATTR(cleave_min);
        NEW_ATTR(cleave_max);
        NEW_ATTR(evasion);
#undef NEW_ATTR

        Simulation s;

        Character dps("DPS", 1, std::make_unique<heads::Greedy>(1, evasion), Attribute(0, 10, 10), Attribute(0, 2, Attribute::noUpperBound));
        dps.addAttribute(bonk_pow, Attribute(1, 10, Attribute::noUpperBound));
        dps.addAttribute(bonk_min, Attribute(0, 1, Attribute::noUpperBound));
        dps.addAttribute(bonk_max, Attribute(0, 5, Attribute::noUpperBound));
        dps.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        dps.addSkill(std::make_unique<skills::SingleAttack>("bonk", skills::RollMode::OneToAttr, bonk_pow, bonk_min, bonk_max));

        Character healer("healer", 1, std::make_unique<heads::Greedy>(1, evasion), Attribute(0, 10, 10), Attribute(0, 2, Attribute::noUpperBound));
        healer.addAttribute(heal_pow, Attribute(1, 10, Attribute::noUpperBound));
        healer.addAttribute(heal_min, Attribute(0, 1, Attribute::noUpperBound));
        healer.addAttribute(heal_max, Attribute(0, 5, Attribute::noUpperBound));
        healer.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        healer.addSkill(std::make_unique<skills::SingleHeal>("heal", skills::RollMode::OneToAttr, heal_pow, heal_min, heal_max, true));

        Character boss("boss", 2, std::make_unique<heads::Greedy>(2, evasion), Attribute(0, 50, 50), Attribute(0, 4, Attribute::noUpperBound));
        boss.addAttribute(cleave_pow, Attribute(1, 10, Attribute::noUpperBound));
        boss.addAttribute(cleave_min, Attribute(0, 1, Attribute::noUpperBound));
        boss.addAttribute(cleave_max, Attribute(0, 3, Attribute::noUpperBound));
        boss.addAttribute(evasion, Attribute(1, 5, Attribute::noUpperBound));
        boss.addSkill(std::make_unique<skills::SingleAttack>("cleave", skills::RollMode::OneToAttr, cleave_pow, cleave_min, cleave_max));

        s.addCharacter(std::move(dps));
        s.addCharacter(std::move(healer));
        s.addCharacter(std::move(boss));
        return s;
    }, 0, 1000000);
    rt.requestInterruption();
    rt.wait();

    // Possible (according to the scheduler) but very unlikely
    EXPECT_LT(rt.percent(), 100.);
}

