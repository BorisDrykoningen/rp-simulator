#include "../core/skills.hpp"
#include "main_menu.hpp"

#include <QCloseEvent>
#include <QMessageBox>

#include <chrono>
#include <iostream>
#include <thread>

static std::unique_ptr<Skill> createAttack(std::string name, const SkillDescription::SerializedData& data) {
    auto rollModeEntry = data.data.find("roll_type");
    if (rollModeEntry == data.data.end()) {
        throw std::logic_error("roll_type not found");
    }
    auto rollMode = std::get<SkillDescription::EnumValue>(rollModeEntry->second).index == 1 ? skills::RollMode::OneToAttr : skills::RollMode::OneToOneHundred;

    auto rollAttrEntry = data.data.find("roll_attr");
    if (rollAttrEntry == data.data.end()) {
        throw std::logic_error("roll_attr not found");
    }

    auto randDmgEntry = data.data.find("rdmg_attr");
    if (randDmgEntry == data.data.end()) {
        throw std::logic_error("rdmg_attr not found");
    }

    auto fixedDmgEntry = data.data.find("fdmg_attr");
    if (fixedDmgEntry == data.data.end()) {
        throw std::logic_error("fdmg_attr not found");
    }

    return std::make_unique<skills::SingleAttack>(
        std::move(name),
        rollMode,
        std::get<std::string>(rollAttrEntry->second),
        std::get<std::string>(randDmgEntry->second),
        std::get<std::string>(fixedDmgEntry->second)
    );
}


static std::unique_ptr<Skill> createHeal(std::string name, const SkillDescription::SerializedData& data) {
    auto rollModeEntry = data.data.find("roll_type");
    if (rollModeEntry == data.data.end()) {
        throw std::logic_error("roll_type not found");
    }
    auto rollMode = std::get<SkillDescription::EnumValue>(rollModeEntry->second).index == 1 ? skills::RollMode::OneToAttr : skills::RollMode::OneToOneHundred;

    auto rollAttrEntry = data.data.find("roll_attr");
    if (rollAttrEntry == data.data.end()) {
        throw std::logic_error("roll_attr not found");
    }

    auto randHeaEntry = data.data.find("rhea_attr");
    if (randHeaEntry == data.data.end()) {
        throw std::logic_error("rhea_attr not found");
    }

    auto fixedHeaEntry = data.data.find("fhea_attr");
    if (fixedHeaEntry == data.data.end()) {
        throw std::logic_error("fhea_attr not found");
    }

    return std::make_unique<skills::SingleHeal>(
        std::move(name),
        rollMode,
        std::get<std::string>(rollAttrEntry->second),
        std::get<std::string>(randHeaEntry->second),
        std::get<std::string>(fixedHeaEntry->second)
    );
}


MainMenu::MainMenu(QWidget* parent):
    QMainWindow(parent)
{
    m_simulation = nullptr;

    m_mainWidget = new QWidget(this);
    setCentralWidget(m_mainWidget);
    m_mainLayout = new QVBoxLayout(m_mainWidget);

    m_editorWidget = new QWidget(this);
    m_mainLayout->addWidget(m_editorWidget);
    m_editorLayout = new QHBoxLayout(m_editorWidget);
    m_characters = new InteractiveList(
        "Perso +",
        "Modifier",
        "Perso -",
        m_editorWidget
    );
    m_editorLayout->addWidget(m_characters);

    m_attrsAndSkills = new QVBoxLayout();
    m_editorLayout->addLayout(m_attrsAndSkills);
    m_attributes = new InteractiveList(
        "Attribut +",
        "Modifier",
        "Attribut -",
        m_editorWidget
    );
    m_attributes->setDisabled(true);
    m_attrsAndSkills->addWidget(m_attributes);
    m_skills = new InteractiveList(
        "Compétence +",
        "Modifier",
        "Compétence -",
        m_editorWidget
    );
    m_skills->setDisabled(true);
    m_attrsAndSkills->addWidget(m_skills);

    m_statusWidget = new QWidget(this);
    m_mainLayout->addWidget(m_statusWidget);
    m_statusLayout = new QHBoxLayout(m_statusWidget);
    m_tasks = new InputLine(this);
    m_tasks->createInput<QSpinBox>("Simulations");
    auto tasks = m_tasks->getInput<QSpinBox>();
    tasks->setMinimum(1);
    tasks->setMaximum(std::numeric_limits<int>::max());
    tasks->setValue(10000);
    m_statusLayout->addWidget(m_tasks);
    m_statusBar = new QProgressBar(m_statusWidget);
    m_statusLayout->addWidget(m_statusBar);
    m_startButton = new QPushButton("Lancer", m_statusWidget);
    m_startButton->setDisabled(true);
    m_statusLayout->addWidget(m_startButton);
    m_cancelButton = new QPushButton("Arrêter", m_statusWidget);
    m_cancelButton->setDisabled(true);
    m_statusLayout->addWidget(m_cancelButton);

    m_characterEditor = new CharacterEditor(this);
    m_attributeEditor = new AttributeEditor(this);
    m_skillEditor = new SkillEditor(this);
    m_skillEditor->add(SkillDescription(
        "Attaque monocible",
        createAttack,
        std::vector<SkillDescription::Entry>{
            {"roll_type", "Type de jet initial", SkillDescription::Enum{{"1D100", "1DX"}}},
            {"roll_attr", "Valeur du jet initial", SkillDescription::String{}},
            {"roll_mods", "Modificateurs initiaux", SkillDescription::Modifiers{}},
            {"fdmg_attr", "Dégâts fixes", SkillDescription::String{}},
            {"fdmg_mods", "Modificateurs des dégâts fixes", SkillDescription::Modifiers{}},
            {"rdmg_attr", "Dégâts aléatoires", SkillDescription::String{}},
            {"rdmg_mods", "Modificateurs des dégâts aléatoires", SkillDescription::Modifiers{}},
        }
    ));
    m_skillEditor->add(SkillDescription(
        "Soin monocible",
        createHeal,
        std::vector<SkillDescription::Entry>{
            {"roll_type", "Type de jet initial", SkillDescription::Enum{{"1D100", "1DX"}}},
            {"roll_attr", "Valeur du jet initial", SkillDescription::String{}},
            {"roll_mods", "Modificateurs initiaux", SkillDescription::Modifiers{}},
            {"fhea_attr", "Soin fixe", SkillDescription::String{}},
            {"fhea_mods", "Modificateurs du soin fixe", SkillDescription::Modifiers{}},
            {"rhea_attr", "Soin aléatoire", SkillDescription::String{}},
            {"rhea_mods", "Modificateurs du soin aléatoire", SkillDescription::Modifiers{}},
        }
    ));

    m_simulation = new SimulationExecuter(m_skillEditor, this);

    connect(m_simulation, &SimulationExecuter::finished, this, &MainMenu::enableEdition);
    connect(m_simulation, &SimulationExecuter::statusUpdate, m_statusBar, &QProgressBar::setValue);
    connect(m_simulation, &SimulationExecuter::resultReady, this, &MainMenu::simulationFinished);

    connect(m_startButton, &QPushButton::clicked, this, &MainMenu::launchSimulation);
    connect(m_cancelButton, &QPushButton::clicked, this, &MainMenu::simulationCancelled);

    connect(m_characters, &InteractiveList::addClicked, this, &MainMenu::addCharacter);
    connect(m_characters, &InteractiveList::modClicked, this, &MainMenu::modCharacter);
    connect(m_characters, &InteractiveList::delClicked, this, &MainMenu::delCharacter);
    connect(m_characters, &InteractiveList::selectionUpdated, this, &MainMenu::onSelectionUpdated);

    connect(m_attributes, &InteractiveList::addClicked, this, &MainMenu::addAttribute);
    connect(m_attributes, &InteractiveList::modClicked, this, &MainMenu::modAttribute);
    connect(m_attributes, &InteractiveList::delClicked, m_attributes, &InteractiveList::remove);

    connect(m_skills, &InteractiveList::addClicked, this, &MainMenu::addSkill);
    connect(m_skills, &InteractiveList::modClicked, this, &MainMenu::modSkill);
    connect(m_skills, &InteractiveList::delClicked, m_skills, &InteractiveList::remove);
}


MainMenu::~MainMenu() noexcept {
    m_simulation->requestInterruption();
    m_simulation->wait();
}


void MainMenu::launchSimulation() {
    disableEdition();
    std::cout << "Simulation launched :)\n";
    // To save the current character before running
    onSelectionUpdated(m_characters->getCurrentIndex(), m_characters->getCurrentIndex());
    auto labels = m_characters->getLabels();
    m_simulation->setup(m_characters->cbegin(), m_characters->cend(), labels.begin(), labels.end());
    m_simulation->setSimulationCount(m_tasks->getInput<QSpinBox>()->value());
    m_simulation->start();
    std::cout << "Asynchronous call ended\n";
}


void MainMenu::simulationCancelled() {
    std::cout << "Simulation cancelled\n";
    m_simulation->requestInterruption();
    m_simulation->wait();
}


void MainMenu::simulationFinished(double victoryRate) {
    std::cout << "Simulation finished with result " << victoryRate << '\n';
    std::ostringstream out;
    out << "L'équipe 1 a " << victoryRate << "% de chances de victoire";

    QMessageBox msgBox(
        QMessageBox::Icon::Information,
        "Résultat de la simulation",
        out.str().c_str(),
        QMessageBox::StandardButton::Ok,
        this
    );
    msgBox.setInformativeText("Ceci est une simulation, les résultats peuvent varier d'un essai à l'autre");
    msgBox.exec();
}


void MainMenu::closeEvent(QCloseEvent* e) {
    beforeClose();
    e->accept();
}


void MainMenu::disableEdition(bool complete) {
    m_startButton->setDisabled(true);
    m_cancelButton->setDisabled(complete);
    m_editorWidget->setDisabled(true);
    std::cout << "Edition disabled\n";
}


void MainMenu::enableEdition() {
    m_startButton->setDisabled(false);
    m_cancelButton->setDisabled(true);
    m_editorWidget->setDisabled(false);
    std::cout << "Edition enabled\n";
}


void MainMenu::addCharacter() {
    CharacterEditor::Data data;
    QVariant variant;
    variant.setValue(data);
    m_characters->add(variant, "Stuff");

    if (m_characters->size() >= 2) {
        m_startButton->setDisabled(false);
    }
}


void MainMenu::modCharacter(int index) {
    disableEdition(true);
    CharacterEditor::Data data = m_characters->getData(index).value<CharacterEditor::Data>();
    m_characterEditor->load(data);
    m_characterEditor->show();
    m_characterEditor->exec();
    m_characterEditor->dump(data);
    QVariant newData;
    newData.setValue(data);
    m_characters->setData(std::move(newData), index);
    enableEdition();
}


void MainMenu::delCharacter(int index) {
    m_characters->remove(index);

    if (m_characters->size() < 2) {
        m_startButton->setDisabled(true);
    }
    if (m_characters->empty()) {
        m_attributes->setDisabled(true);
        m_skills->setDisabled(true);
    }
}


void MainMenu::addAttribute() {
    std::cout << "Attribute added\n";
    AttributeEditor::Data data;
    QVariant variant;
    variant.setValue(data);
    m_attributes->add(std::move(variant), "Attribut");
}


void MainMenu::modAttribute(int index) {
    disableEdition(true);
    AttributeEditor::Data data = m_attributes->getData(index).value<AttributeEditor::Data>();
    m_attributeEditor->load(data);
    m_attributeEditor->show();
    m_attributeEditor->exec();
    m_attributeEditor->dump(data);
    QVariant newData;
    newData.setValue(data);
    m_attributes->setData(std::move(newData), index);
    enableEdition();
}


void MainMenu::addSkill() {
    std::cout << "Skill added\n";
    m_skills->add(QVariant(), "Compétence");
}


void MainMenu::modSkill(int index) {
    disableEdition(true);
    auto variant = m_skills->getData(index);
    auto data = variant.value<SkillDescription::SerializedData>();
    std::cout << "From " << data.identifier << '\n';
    try {
        m_skillEditor->deserialize(data);
    } catch (const std::invalid_argument& err) {
        std::cerr << "[WARNING] " << err.what() << '\n';
    }
    m_skillEditor->show();
    m_skillEditor->exec();
    data = m_skillEditor->serialize();
    std::cout << "To " << data.identifier << '\n';
    variant.setValue(data);
    m_skills->setData(variant, index);
    enableEdition();
}


void MainMenu::onSelectionUpdated(int oldIndex, int newIndex) {
    std::cout << "Selection updated; old: " << oldIndex << ", new: " << newIndex << '\n';

    if (newIndex >= 0) {
        m_attributes->setDisabled(false);
        m_skills->setDisabled(false);
    }

    auto attributesNames = m_attributes->getLabels();
    auto attributesData = m_attributes->clear();
    auto skillsNames = m_skills->getLabels();
    auto skillsData = m_skills->clear();
    assert(attributesNames.size() == attributesData.size());
    assert(skillsNames.size() == skillsData.size());

    std::cout << "Old: " << attributesNames.size() << " attributes, " << skillsNames.size() << " skills\n";

    // We may want to update the formely edited character (if any). If oldIndex
    // == -1, it means the former item was deleted!
    if (oldIndex >= 0 && oldIndex < m_characters->size()) {
        // Fetch data from m_characters
        const auto& var = m_characters->getData(oldIndex);
        auto data = var.value<CharacterEditor::Data>();

        // Update it with m_attributes and m_skills
        data.attributesNames = std::move(attributesNames);
        data.attributesData = std::move(attributesData);
        data.skillsNames = std::move(skillsNames);
        data.skillsData = std::move(skillsData);

        // Record it in m_characters
        QVariant newVar;
        newVar.setValue(data);
        m_characters->setData(std::move(newVar), oldIndex);

        std::cout << "old data recorded\n";
    }

    // We load the appropriate values in the new one
    if (newIndex >= 0 && newIndex < m_characters->size()) {
        const auto& var = m_characters->getData(newIndex);
        const auto data = var.value<CharacterEditor::Data>();

        const auto attrCount = data.attributesNames.size();
        assert(attrCount == data.attributesData.size());
        for (size_t i = 0; i < attrCount; ++i) {
            m_attributes->add(data.attributesData[i], data.attributesNames[i]);
        }

        const auto skillCount = data.skillsNames.size();
        assert(skillCount == data.skillsData.size());
        for (size_t i = 0; i < skillCount; ++i) {
            m_skills->add(data.skillsData[i], data.skillsNames[i]);
        }

        std::cout << "new data loaded\n";
    }
}

