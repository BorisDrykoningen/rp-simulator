#ifndef SRC_INTERFACE_CHARACTER_EDITOR_HPP
#define SRC_INTERFACE_CHARACTER_EDITOR_HPP

#include "../core/attribute.hpp"
#include "../core/team.hpp"
#include "input_line.hpp"

#include <QCheckBox>
#include <QDialog>
#include <QSpinBox>

#include <vector>


class CharacterEditor: public QDialog {
    Q_OBJECT

    QVBoxLayout* m_layout;

    InputLine* m_team;
    InputLine* m_minHp;
    InputLine* m_initHp;
    InputLine* m_maxHp;

public:
    CharacterEditor(QWidget* parent = nullptr);

    struct Data {
        TeamId team;
        Attribute::IntType minHp;
        bool hasMinHp;
        Attribute::IntType initHp;
        bool hasInitHp;
        Attribute::IntType maxHp;
        bool hasMaxHp;
        // Added here for convenience. But the character editor doesn't handle
        // attributes and skills
        std::vector<QString> attributesNames;
        std::vector<QVariant> attributesData;
        std::vector<QString> skillsNames;
        std::vector<QVariant> skillsData;

        Data();
    };

    void load(const Data& data);
    void dump(Data& data) const;
};

Q_DECLARE_METATYPE(CharacterEditor::Data)

#endif

