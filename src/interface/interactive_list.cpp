#include "interactive_list.hpp"

#include <algorithm>


InteractiveList::InteractiveList(QString addLabel, QString modLabel, QString delLabel, QWidget* parent): QWidget(parent) {
    m_oldIndex = -1;
    m_removeOp = false;

    m_mainLayout = new QVBoxLayout(this);
    m_list = new QListWidget(this);
    m_list->setSelectionMode(QAbstractItemView::SingleSelection);
    m_list->setDragEnabled(true);
    m_list->viewport()->setAcceptDrops(true);
    m_list->setDropIndicatorShown(true);
    m_list->setDragDropMode(QAbstractItemView::InternalMove);
    m_mainLayout->addWidget(m_list);
    m_buttonsLayout = new QHBoxLayout();
    m_mainLayout->addLayout(m_buttonsLayout);
    m_buttonAdd = new QPushButton(addLabel, this);
    m_buttonsLayout->addWidget(m_buttonAdd);
    if (modLabel.isEmpty()) {
        m_buttonMod = nullptr;
    } else {
        m_buttonMod = new QPushButton(modLabel, this);
        m_buttonsLayout->addWidget(m_buttonMod);
    }
    m_buttonDel = new QPushButton(delLabel, this);
    m_buttonsLayout->addWidget(m_buttonDel);

    connect(m_buttonAdd, &QPushButton::clicked, this, &InteractiveList::addClicked);
    if (m_buttonMod) {
        connect(m_buttonMod, &QPushButton::clicked, this, &InteractiveList::raiseModClicked);
    }
    connect(m_buttonDel, &QPushButton::clicked, this, &InteractiveList::raiseDelClicked);

    connect(m_list, &QListWidget::itemSelectionChanged, this, &InteractiveList::raiseSelectionUpdated);

    connect(m_list->model(), &QAbstractItemModel::rowsMoved, this, &InteractiveList::onRowsMoved);
}


void InteractiveList::raiseModClicked() {
    try {
        modClicked(getCurrentIndex());
    } catch (const std::runtime_error&) {}
}


void InteractiveList::raiseDelClicked() {
    try {
        delClicked(getCurrentIndex());
    } catch (const std::runtime_error&) {}
}


void InteractiveList::raiseSelectionUpdated() {
    if (m_removeOp) {
        return;
    }

    int newIndex = -1;
    try {
        newIndex = getCurrentIndex();
    } catch (const std::runtime_error&) {}

    // If the new index doesn't exist, the signal is raised with newIndex = -1
    selectionUpdated(m_oldIndex, newIndex);
    m_oldIndex = newIndex;
}


void InteractiveList::add(QVariant obj, const QString& label) {
    m_data.emplace_back(std::move(obj));
    try {
        m_list->addItem(label);
    } catch (...) {
        // Strong exception guarantee. There must always be pairs of data and
        // label. If one insertion fails, we must remove the already-inserted
        // data
        m_data.erase(m_data.end() - 1);
        throw;
    }

    // Make items editable
    auto item = m_list->item(m_list->count() - 1);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
}


int InteractiveList::getCurrentIndex() const {
    const auto index = m_list->currentIndex().row();
    if (index < 0 || static_cast<size_t>(index) >= m_data.size()) {
        throw std::runtime_error("InteractiveList is empty");
    }
    return index;
}


const QVariant& InteractiveList::getData(int index) const {
    if (index < 0 || static_cast<size_t>(index) >= m_data.size()) {
        throw std::invalid_argument("Invalid index");
    }
    return m_data[index];
}


void InteractiveList::setData(QVariant data, int index) {
    if (index < 0 || static_cast<size_t>(index) >= m_data.size()) {
        throw std::invalid_argument("Invalid index");
    }
    m_data[index] = std::move(data);
}


std::vector<QString> InteractiveList::getLabels() const {
    std::vector<QString> res;
    const int count = m_list->count();
    for (int i = 0; i < count; ++i) {
        res.emplace_back(m_list->item(i)->text());
    }

    return res;
}


int InteractiveList::size() const {
    return m_data.size();
}


bool InteractiveList::empty() const {
    return m_data.empty();
}


std::vector<QVariant> InteractiveList::clear() noexcept {
    // Fuck, if it fails, I can't unremove data
    m_list->clear();
    std::vector<QVariant> res;
    m_data.swap(res);
    return res;
}


InteractiveList::iterator InteractiveList::begin() {
    return m_data.begin();
}


InteractiveList::iterator InteractiveList::end() {
    return m_data.end();
}


InteractiveList::const_iterator InteractiveList::begin() const {
    return m_data.begin();
}


InteractiveList::const_iterator InteractiveList::end() const {
    return m_data.end();
}


InteractiveList::const_iterator InteractiveList::cbegin() const {
    return begin();
}


InteractiveList::const_iterator InteractiveList::cend() const {
    return end();
}


void InteractiveList::remove(int index) {
    // Ignoring selection update signals because Qt makes it behavior weird
    m_removeOp = true;
    QListWidgetItem* item;
    try {
        item = m_list->takeItem(index);
    } catch (...) {
        m_removeOp = false;
        throw;
    }
    m_removeOp = false;

    if (item) {
        delete item;

        // As we could take item and delete it, it is a valid index. No need to
        // check
        m_data.erase(m_data.begin() + index);

        // Invaliding the old index (it has been removed)
        m_oldIndex = -1;
        // Raising the signal with appropriate values
        raiseSelectionUpdated();
    }
}


void InteractiveList::onRowsMoved(const QModelIndex& srcParent, int srcBegin, int srcEnd, const QModelIndex& dstParent, int dstBegin) {
    // Registering data
    selectionUpdated(m_oldIndex, -1);
    m_oldIndex = -1;

    // Starting to move data
    if (srcParent.isValid() || dstParent.isValid()) {
        throw std::invalid_argument("Move is supported only from root to root");
    }

    const auto elementCount = srcEnd - srcBegin + 1;
    if (srcBegin == dstBegin) {
        std::cout << "Nothing to do :)\n";
        return;
    }

    // This algorithm is probably a slug because it allocates temporary memory.
    // I don't give a fuck
    std::vector<QVariant> cpy;
    cpy.resize(elementCount);
    const auto first = m_data.cbegin() + srcBegin;
    const auto last = m_data.cbegin() + srcEnd + 1;
    std::move(first, last, cpy.begin());
    if (srcBegin < dstBegin) {
        std::cout << "Inserting after\n";
        m_data.erase(first, last);
        m_data.insert(m_data.cbegin() + dstBegin - elementCount, cpy.cbegin(), cpy.cend());
    } else {
        std::cout << "Inserting before\n";
        m_data.erase(first, last);
        m_data.insert(m_data.cbegin() + dstBegin, cpy.cbegin(), cpy.cend());
    }

    // Loading data
    raiseSelectionUpdated();
}

