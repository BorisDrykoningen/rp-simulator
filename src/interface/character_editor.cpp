#include "character_editor.hpp"
#include "utils.hpp"


CharacterEditor::CharacterEditor(QWidget* parent): QDialog(parent) {
    m_layout = new QVBoxLayout(this);

    m_team = new InputLine(this);
    m_team->createInput<QSpinBox>("Équipe");
    m_layout->addWidget(m_team);

    m_minHp = new InputLine(this);
    m_minHp->createInput<QSpinBox>("PV min", std::optional(Qt::CheckState::Unchecked));
    setupForAttribute(m_minHp->getInput<QSpinBox>());
    m_layout->addWidget(m_minHp);

    m_initHp = new InputLine(this);
    m_initHp->createInput<QSpinBox>("PV au début", std::optional(Qt::CheckState::Unchecked));
    setupForAttribute(m_initHp->getInput<QSpinBox>());
    m_layout->addWidget(m_initHp);

    m_maxHp = new InputLine(this);
    m_maxHp->createInput<QSpinBox>("PV max", std::optional(Qt::CheckState::Checked));
    setupForAttribute(m_maxHp->getInput<QSpinBox>());
    m_layout->addWidget(m_maxHp);
}


void CharacterEditor::load(const CharacterEditor::Data& data) {
    m_team->getInput<QSpinBox>()->setValue(data.team);
    m_minHp->getInput<QSpinBox>()->setValue(data.minHp);
    m_minHp->setCheckState(checked(data.hasMinHp));
    m_initHp->getInput<QSpinBox>()->setValue(data.initHp);
    m_initHp->setCheckState(checked(data.hasInitHp));
    m_maxHp->getInput<QSpinBox>()->setValue(data.maxHp);
    m_maxHp->setCheckState(checked(data.hasMaxHp));
}


void CharacterEditor::dump(CharacterEditor::Data& data) const {
    data.team = m_team->getInput<QSpinBox>()->value();
    data.minHp = m_minHp->getInput<QSpinBox>()->value();
    data.hasMinHp = m_minHp->getCheckState() != Qt::CheckState::Unchecked;
    data.initHp = m_initHp->getInput<QSpinBox>()->value();
    data.hasInitHp = m_initHp->getCheckState() != Qt::CheckState::Unchecked;
    data.maxHp = m_maxHp->getInput<QSpinBox>()->value();
    data.hasMaxHp = m_maxHp->getCheckState() != Qt::CheckState::Unchecked;
}


CharacterEditor::Data::Data():
    team(1),
    minHp(0),
    hasMinHp(false),
    initHp(0),
    hasInitHp(false),
    maxHp(10),
    hasMaxHp(true)
{}

