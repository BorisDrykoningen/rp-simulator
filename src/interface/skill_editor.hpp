#ifndef SRC_INTERFACE_SKILL_EDITOR_HPP
#define SRC_INTERFACE_SKILL_EDITOR_HPP

#include "input_line.hpp"
#include "auto_form.hpp"

#include <QDialog>

#include <vector>


class SkillEditor: public QDialog {
    Q_OBJECT

    QVBoxLayout* m_layout;
    InputLine* m_skillType;
    AutoForm* m_form;
    bool m_indexChangeLocked;

    std::vector<SkillDescription> m_descs;

private slots:
    void onIndexChange(int index);

public:
    SkillEditor(QWidget* parent = nullptr);
    void add(SkillDescription desc);
    SkillDescription::SerializedData serialize();
    void deserialize(const SkillDescription::SerializedData& data);
    void reset();
    std::unique_ptr<Skill> fromDesc(std::string name, const SkillDescription::SerializedData& desc);
};

#endif

