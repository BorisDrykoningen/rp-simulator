#ifndef SRC_INTERFACE_AUTO_FORM_HPP
#define SRC_INTERFACE_AUTO_FORM_HPP

#include "../core/skill_description.hpp"

#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QWidget>

#include <optional>
#include <variant>
#include <vector>


class AutoForm: public QWidget {
    Q_OBJECT

    QGridLayout* m_layout;

    SkillDescription m_desc;
    std::vector<QWidget*> m_widgets;
    std::vector<QLabel*> m_labels;

    std::map<std::string, SkillDescription::Value> data() const;

    void addLine(QString label, SkillDescription::ValueType valueType);

public:
    AutoForm(SkillDescription desc, QWidget* parent = nullptr);

    SkillDescription::SerializedData serialize() const;
    void deserialize(const SkillDescription::SerializedData& data);

private slots:
    void addModifierClicked();
};

Q_DECLARE_METATYPE(SkillDescription::SerializedData);

#endif

