#include "auto_form.hpp"
#include "interactive_list.hpp"
#include "utils.hpp"

#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>


AutoForm::AutoForm(SkillDescription desc, QWidget* parent): QWidget(parent), m_desc(std::move(desc)) {
    m_layout = new QGridLayout(this);
    for (const auto& i: m_desc.m_desc) {
        addLine(i.label.c_str(), i.type);
    }
}


void AutoForm::addLine(QString label, SkillDescription::ValueType valueType) {
    QWidget* input = nullptr;
    try {
        const auto conf = std::get<SkillDescription::Integer>(valueType);
        QSpinBox* spinBox = new QSpinBox(this);
        if (conf.min.has_value()) {
            spinBox->setMinimum(conf.min.value());
        } else {
            spinBox->setMinimum(spinBoxAttrMin);
        }
        if (conf.max.has_value()) {
            spinBox->setMaximum(conf.max.value());
        } else {
            spinBox->setMaximum(spinBoxAttrMax);
        }

        input = spinBox;
    } catch (const std::bad_variant_access&) {
        try {
            [[maybe_unused]] const auto conf = std::get<SkillDescription::String>(valueType);
            input = new QLineEdit(this);
        } catch (const std::bad_variant_access&) {
            try {
                const auto conf = std::get<SkillDescription::Enum>(valueType);
                auto* comboBox = new QComboBox(this);
                for (const auto& i: conf.values) {
                    comboBox->addItem(i.c_str());
                }

                input = comboBox;
            } catch (const std::bad_variant_access&) {
                try {
                    [[maybe_unused]] const auto conf = std::get<SkillDescription::Modifiers>(valueType);
                    QString addLabel = "Ajouter";
                    QString modLabel = "";
                    QString delLabel = "Supprimer";
                    auto* list = new InteractiveList(addLabel, modLabel, delLabel, this);
                    connect(list, &InteractiveList::addClicked, this, &AutoForm::addModifierClicked);
                    connect(list, &InteractiveList::delClicked, list, &InteractiveList::remove);

                    input = list;
                } catch (const std::bad_variant_access&) {
                    throw std::logic_error("You forgot to update AutoForm::addLine!\n");
                }
            }
        }
    }

    const auto row = m_layout->rowCount();
    auto labelWidget = new QLabel(label);
    m_layout->addWidget(labelWidget, row, 0);
    m_layout->addWidget(input, row, 1);

    m_widgets.emplace_back(input);
    m_labels.emplace_back(labelWidget);
}


std::map<std::string, SkillDescription::Value> AutoForm::data() const {
    std::map<std::string, SkillDescription::Value> res;

    for (size_t lineNum = 0; lineNum < m_desc.m_desc.size(); ++lineNum) {
        auto input = m_widgets[lineNum];
        const auto& entry = m_desc.m_desc[lineNum];
        const auto& type = entry.type;
        const auto& lineName = entry.name;

        try {
            [[maybe_unused]] const auto conf = std::get<SkillDescription::Integer>(type);
            auto spinBox = qobject_cast<QSpinBox*>(input);
            res.emplace(lineName, spinBox->value());
        } catch (const std::bad_variant_access&) {
            try {
                [[maybe_unused]] const auto conf = std::get<SkillDescription::String>(type);
                auto lineEdit = qobject_cast<QLineEdit*>(input);
                res.emplace(lineName, lineEdit->text().toStdString());
            } catch (const std::bad_variant_access&) {
                try {
                    [[maybe_unused]] const auto conf = std::get<SkillDescription::Enum>(type);
                    auto comboBox = qobject_cast<QComboBox*>(input);
                    auto index = comboBox->currentIndex();
                    if (index < 0) {
                        throw std::runtime_error("Not selected yet");
                    }
                    auto label = comboBox->currentText();
                    res.emplace(lineName, SkillDescription::EnumValue{label.toStdString(), static_cast<size_t>(index)});
                } catch (const std::bad_variant_access&) {
                    try {
                        [[maybe_unused]] const auto conf = std::get<SkillDescription::Modifiers>(type);
                        auto list = qobject_cast<InteractiveList*>(input);
                        std::vector<std::string> mods;
                        for (const QString& i: list->getLabels()) {
                            mods.emplace_back(i.toStdString());
                        }
                        res.emplace(lineName, SkillDescription::ModifiersValue{std::move(mods)});
                    } catch (const std::bad_variant_access&) {
                        throw std::logic_error("You forgot to update AutoForm::data!\n");
                    }
                }
            }
        }
    }

    return res;
}


SkillDescription::SerializedData AutoForm::serialize() const {
    return SkillDescription::SerializedData{m_desc.m_identifier, data()};
}


void AutoForm::deserialize(const SkillDescription::SerializedData& data) {
    if (data.identifier != m_desc.m_identifier) {
        throw std::runtime_error("AutoForm's model not updated");
    }

    for (size_t i = 0; i < m_desc.m_desc.size(); ++i) {
        const auto& entry = m_desc.m_desc[i];
        const auto& lineName = entry.name;
        auto found = data.data.find(lineName);
        if (found == data.data.end()) {
            throw std::invalid_argument(lineName + " not found");
        }
        const auto& value = found->second;

        try {
            const auto ival = std::get<Attribute::IntType>(value);
            auto spinBox = qobject_cast<QSpinBox*>(m_widgets[i]);
            if (spinBox == nullptr) {
                throw std::invalid_argument(lineName + " is not an integer");
            }
            spinBox->setValue(ival);
        } catch (const std::bad_variant_access&) {
            try {
                const auto& sval = std::get<std::string>(value);
                auto lineEdit = qobject_cast<QLineEdit*>(m_widgets[i]);
                if (lineEdit == nullptr) {
                    throw std::invalid_argument(lineName + " is not a string");
                }
                lineEdit->setText(sval.c_str());
            } catch (const std::bad_variant_access&) {
                try {
                    const auto& eval = std::get<SkillDescription::EnumValue>(value);
                    auto combo = qobject_cast<QComboBox*>(m_widgets[i]);
                    if (combo == nullptr) {
                        throw std::invalid_argument(lineName + " is not an enum");
                    }
                    combo->setCurrentIndex(eval.index);
                    if (combo->currentText() != eval.name.c_str()) {
                        throw std::invalid_argument(eval.name + " doesn't match the model");
                    }
                } catch (const std::bad_variant_access&) {
                    try {
                        const auto& mval = std::get<SkillDescription::ModifiersValue>(value);
                        auto list = qobject_cast<InteractiveList*>(m_widgets[i]);
                        if (list == nullptr) {
                            throw std::invalid_argument(lineName + " is not a modifiers list");
                        }
                        list->clear();
                        for (const auto& i: mval.values) {
                            list->add(QVariant(0), i.c_str());
                        }
                    } catch (const std::bad_variant_access&) {
                        throw std::logic_error("You forgot to update AutoForm::deserialize");
                    }
                }
            }
        }
    }

    m_layout->activate();
}


void AutoForm::addModifierClicked() {
    auto list = qobject_cast<InteractiveList*>(sender());
    list->add(QVariant(0));
}

