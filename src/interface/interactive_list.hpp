#ifndef SRC_INTERFACE_INTERACTIVE_LIST_HPP
#define SRC_INTERFACE_INTERACTIVE_LIST_HPP

#include <QLayout>
#include <QListWidget>
#include <QPushButton>

#include <iostream>
#include <vector>


class InteractiveList: public QWidget {
    Q_OBJECT

    int m_oldIndex;
    bool m_removeOp;
    QVBoxLayout* m_mainLayout;
    QListWidget* m_list;
    QHBoxLayout* m_buttonsLayout;
    QPushButton* m_buttonAdd;
    QPushButton* m_buttonMod;
    QPushButton* m_buttonDel;
    std::vector<QVariant> m_data;

public:
    InteractiveList(QString addLabel, QString modLabel, QString delLabel, QWidget* parent = nullptr);

    using iterator = std::vector<QVariant>::iterator;
    using const_iterator = std::vector<QVariant>::const_iterator;

private slots:
    void raiseModClicked();
    void raiseDelClicked();
    void raiseSelectionUpdated();

signals:
    void addClicked();
    void modClicked(int currentItem);
    void delClicked(int currentItem);
    void selectionUpdated(int oldItem, int newItem);

public:
    void add(QVariant obj, const QString& label = QString());
    int getCurrentIndex() const;

    const QVariant& getData(int index) const;
    void setData(QVariant value, int index);

    std::vector<QString> getLabels() const;

    int size() const;
    bool empty() const;
    std::vector<QVariant> clear() noexcept;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;

public slots:
    void remove(int index);
    void onRowsMoved(const QModelIndex& srcParent, int srcBegin, int srcEnd, const QModelIndex& dstParent, int dstBegin);
};

#endif

