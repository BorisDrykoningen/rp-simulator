#include "../core/heads.hpp"
#include "../core/skill_description.hpp"
#include "../core/skills.hpp"
#include "attribute_editor.hpp"
#include "character_editor.hpp"
#include "simulation_executer.hpp"
#include "skill_editor.hpp"

#include <QMetaType>

#include <iostream>
#include <thread>


void SimulationExecuter::run() {
    std::cout << "Running\n";

    // This example is directly copied from the tests
    m_runtime.run([this]() -> Simulation {
        std::lock_guard guard(m_configLock);
        Simulation s;

        auto di = m_data.begin();
        auto ni = m_names.begin();
        while (di != m_data.end()) {
            assert(ni != m_names.end());

            // Setting-up the character's basic data
            const auto i = di->value<CharacterEditor::Data>();
            Character c(
                ni->toStdString(),
                i.team,
                // TODO: different heads, configurable heads
                std::make_unique<heads::Greedy>(),
                Attribute(
                    i.hasMinHp ? i.minHp : Attribute::noLowerBound,
                    i.hasInitHp ? i.initHp : (i.hasMaxHp ? i.maxHp : throw std::invalid_argument("Pas de données sur le nombre de PV initiaux")),
                    i.hasMaxHp ? i.maxHp : Attribute::noUpperBound
                ),
                // TODO: configurable actions
                Attribute(0, 1, 1)
            );

            // Adding Attributes
            auto dj = i.attributesData.cbegin();
            auto nj = i.attributesNames.cbegin();
            while (dj != i.attributesData.cend()) {
                assert(nj != i.attributesNames.cend());

                const auto j = dj->value<AttributeEditor::Data>();
                Attribute attr(
                    j.hasMin ? j.min : Attribute::noLowerBound,
                    j.init,
                    j.hasMax ? j.max : Attribute::noUpperBound
                );
                c.addAttribute(nj->toStdString(), std::move(attr));

                ++dj;
                ++nj;
            }

            // Adding Skills
            dj = i.skillsData.cbegin();
            nj = i.skillsNames.cbegin();
            while (dj != i.skillsData.end()) {
                assert(nj != i.skillsNames.end());

                auto name = nj->toStdString();
                auto skillArgs = dj->value<SkillDescription::SerializedData>();
                auto skill = m_editor->fromDesc(std::move(name), skillArgs);
                c.addSkill(std::move(skill));

                ++dj;
                ++nj;
            }

            // Now that Attributes and Skills are added, add the Character to
            // the Simulation
            s.addCharacter(std::move(c));

            ++di;
            ++ni;
        }

        return s;
    }, 0, m_simulationCount);

    while (m_runtime.completed() < m_simulationCount && !isInterruptionRequested()) {
        statusUpdate(m_runtime.percent());
        QThread::msleep(100);
    }

    if (isInterruptionRequested()) {
        std::cout << "Interruption caught in worker\n";
        m_runtime.requestInterruption();
        m_runtime.wait();
        statusUpdate(0.);
    } else {
        std::cout << "End of task reached\n";
        m_runtime.wait();
        statusUpdate(100.);
        resultReady(m_runtime.victoryRate() * 100.);
    }
}


SimulationExecuter::SimulationExecuter(SkillEditor* editor, QObject* parent):
    QThread(parent),
    m_runtime(1),
    m_editor(editor),
    m_simulationCount(100000)
{}


SimulationExecuter::~SimulationExecuter() noexcept {}


void SimulationExecuter::setup(
        InteractiveList::const_iterator dataBegin,
        InteractiveList::const_iterator dataEnd,
        std::vector<QString>::const_iterator namesBegin,
        std::vector<QString>::const_iterator namesEnd
) {
    m_data = std::vector(dataBegin, dataEnd);
    m_names = std::vector(namesBegin, namesEnd);
}


void SimulationExecuter::setSimulationCount(size_t count) {
    m_simulationCount = count;
}

