#include "attribute_editor.hpp"
#include "utils.hpp"

#include <QSpinBox>


AttributeEditor::AttributeEditor(QWidget* parent): QDialog(parent) {
    m_layout = new QVBoxLayout(this);
    m_min = new InputLine(this);
    m_min->createInput<QSpinBox>("Valeur min", std::optional(Qt::CheckState::Checked));
    setupForAttribute(m_min->getInput<QSpinBox>());
    m_layout->addWidget(m_min);

    m_init = new InputLine(this);
    m_init->createInput<QSpinBox>("Valeur initiale");
    setupForAttribute(m_init->getInput<QSpinBox>());
    m_layout->addWidget(m_init);

    m_max = new InputLine(this);
    m_max->createInput<QSpinBox>("Valeur max", std::optional(Qt::CheckState::Unchecked));
    setupForAttribute(m_max->getInput<QSpinBox>());
    m_layout->addWidget(m_max);
}


void AttributeEditor::load(const AttributeEditor::Data& data) {
    m_min->setCheckState(checked(data.hasMin));
    m_min->getInput<QSpinBox>()->setValue(data.min);

    m_init->getInput<QSpinBox>()->setValue(data.init);

    m_max->setCheckState(checked(data.hasMax));
    m_max->getInput<QSpinBox>()->setValue(data.max);
}


void AttributeEditor::dump(AttributeEditor::Data& data) const {
    data.min = m_min->getInput<QSpinBox>()->value();
    data.hasMin = m_min->getCheckState() != Qt::CheckState::Unchecked;
    data.init = m_init->getInput<QSpinBox>()->value();
    data.max = m_max->getInput<QSpinBox>()->value();
    data.hasMax = m_max->getCheckState() != Qt::CheckState::Unchecked;
}


AttributeEditor::Data::Data():
    min(1),
    hasMin(true),
    init(10),
    max(95),
    hasMax(false)
{}

