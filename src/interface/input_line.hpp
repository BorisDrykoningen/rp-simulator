#ifndef SRC_INTERFACE_INPUT_LINE_HPP
#define SRC_INTERFACE_INPUT_LINE_HPP

#include <QCheckBox>
#include <QLabel>
#include <QLayout>

#include <cassert>
#include <optional>


class InputLine: public QWidget {
    Q_OBJECT

    QHBoxLayout* m_layout;
    QLabel* m_label;
    QCheckBox* m_checkbox;
    QWidget* m_input;

public:
    InputLine(QWidget* parent = nullptr);

    template <typename In, typename... Args>
    void createInput(
        const QString& label,
        std::optional<Qt::CheckState> checkState = std::optional<Qt::CheckState>(),
        Args&&... args
    ) {
        assert(!m_input);

        m_layout = new QHBoxLayout(this);
        m_label = new QLabel(label, this);
        m_layout->addWidget(m_label);
        m_input = new In(std::forward<Args>(args)..., this);
        m_layout->addWidget(m_input);

        if (checkState.has_value()) {
            m_checkbox = new QCheckBox(this);
            m_checkbox->setCheckState(checkState.value());
            m_layout->addWidget(m_checkbox);

            connect(m_checkbox, &QCheckBox::stateChanged, this, &InputLine::onCheckStateChange);
            onCheckStateChange(checkState.value());
        }
    }


    template <typename In>
    In* getInput() {
        return qobject_cast<In*>(m_input);
    }


    template <typename In>
    const In* getInput() const {
        return qobject_cast<const In*>(m_input);
    }


    Qt::CheckState getCheckState() const;
    void setCheckState(Qt::CheckState state);


public slots:
    void onCheckStateChange(int state);
};


#endif

