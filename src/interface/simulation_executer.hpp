#ifndef SRC_INTERFACE_SIMULATION_EXECUTER_HPP
#define SRC_INTERFACE_SIMULATION_EXECUTER_HPP

#include "../runtime/threaded_runtime.hpp"
#include "interactive_list.hpp"

#include <QThread>


class SkillEditor;

class SimulationExecuter: public QThread {
    Q_OBJECT

    ThreadedRuntime m_runtime;
    SkillEditor* m_editor;
    size_t m_simulationCount;
    std::mutex m_configLock;
    std::vector<QVariant> m_data;
    std::vector<QString> m_names;

protected:
    void run() override;

public:
    SimulationExecuter(SkillEditor* editor, QObject* parent = nullptr);
    ~SimulationExecuter() noexcept override;

    void setup(
        InteractiveList::const_iterator dataBegin,
        InteractiveList::const_iterator dataEnd,
        std::vector<QString>::const_iterator namesBegin,
        std::vector<QString>::const_iterator namesEnd
    );
    void setSimulationCount(size_t count);

signals:
    void statusUpdate(double percentage);
    void resultReady(double victoryRatePercent);
};

#endif

