#ifndef SRC_INTERFACE_ATTRIBUTE_EDITOR_HPP
#define SRC_INTERFACE_ATTRIBUTE_EDITOR_HPP

#include "../core/attribute.hpp"
#include "input_line.hpp"

#include <QDialog>


class AttributeEditor: public QDialog {
    Q_OBJECT

    QVBoxLayout* m_layout;
    InputLine* m_min;
    InputLine* m_init;
    InputLine* m_max;

public:
    AttributeEditor(QWidget* parent = nullptr);

    struct Data {
        Attribute::IntType min;
        bool hasMin;
        Attribute::IntType init;
        Attribute::IntType max;
        bool hasMax;

        Data();
    };

    void load(const Data& data);
    void dump(Data& data) const;
};


Q_DECLARE_METATYPE(AttributeEditor::Data)

#endif

