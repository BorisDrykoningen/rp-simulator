#include "skill_editor.hpp"

#include <QComboBox>

#include <iostream>


void SkillEditor::reset() {
    if (m_form) {
        m_layout->removeWidget(m_form);
        m_layout->activate();
        m_form->deleteLater();
        m_form = nullptr;
    }
    m_skillType->getInput<QComboBox>()->setCurrentIndex(-1);
}


void SkillEditor::onIndexChange(int index) {
    if (m_indexChangeLocked) {
        return;
    }

    std::cout << "SkillEditor::onIndexChange(" << index << ")\n";
    std::cout << "m_form = " << m_form << '\n';

    if (m_form) {
        m_layout->removeWidget(m_form);
        m_form->deleteLater();
        m_form = nullptr;
        m_layout->activate();
    }
    if (index >= 0) {
        const auto& newDesc = m_descs[index];
        m_form = new AutoForm(newDesc, this);
        m_layout->addWidget(m_form);
    }

    // Recalculating the layout
    m_layout->activate();
}


SkillEditor::SkillEditor(QWidget* parent): QDialog(parent) {
    m_layout = new QVBoxLayout(this);
    m_skillType = new InputLine(this);
    m_skillType->createInput<QComboBox>("Type de compétence");
    m_layout->addWidget(m_skillType);
    m_layout->activate();
    m_form = nullptr;
    m_indexChangeLocked = false;

    connect(m_skillType->getInput<QComboBox>(), &QComboBox::currentIndexChanged, this, &SkillEditor::onIndexChange);
}


void SkillEditor::add(SkillDescription desc) {
    m_descs.emplace_back(std::move(desc));
    try {
        m_indexChangeLocked = true;
        auto comboBox = m_skillType->getInput<QComboBox>();
        comboBox->addItem(m_descs.back().displayName());
        comboBox->setCurrentIndex(-1);
        m_indexChangeLocked = false;
    } catch (...) {
        m_descs.pop_back();
        m_indexChangeLocked = false;
        throw;
    }
}


SkillDescription::SerializedData SkillEditor::serialize() {
    auto res = m_form->serialize();
    reset();
    return res;
}


void SkillEditor::deserialize(const SkillDescription::SerializedData& data) {
    if (m_form) {
        std::cerr << "[WARNING] m_form should be null\n";
        reset();
    }
    std::cout << "Updating AutoForm\n";
    auto found = std::find_if(m_descs.begin(), m_descs.end(), [&data](const auto& i) {
        return i.recognizes(data);
    });
    if (found == m_descs.end()) {
        throw std::invalid_argument(data.identifier + " does not match a model");
    }

    m_form = new AutoForm(*found, this);
    m_form->deserialize(data);
    m_layout->addWidget(m_form);

    auto index = found - m_descs.begin();
    m_indexChangeLocked = true;
    m_skillType->getInput<QComboBox>()->setCurrentIndex(index);
    m_indexChangeLocked = false;
}


std::unique_ptr<Skill> SkillEditor::fromDesc(std::string name, const SkillDescription::SerializedData& desc) {
    for (auto& i: m_descs) {
        if (i.recognizes(desc)) {
            return i.createSkill(std::move(name), desc);
        }
    }

    throw std::logic_error("Invalid data");
}

