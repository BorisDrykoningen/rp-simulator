#include "input_line.hpp"


InputLine::InputLine(QWidget* parent):
    QWidget(parent),
    m_layout(nullptr),
    m_label(nullptr),
    m_checkbox(nullptr),
    m_input(nullptr)
{}


Qt::CheckState InputLine::getCheckState() const {
    return m_checkbox ? m_checkbox->checkState() : Qt::CheckState::Unchecked;
}


void InputLine::setCheckState(Qt::CheckState state) {
    assert(m_checkbox);
    m_checkbox->setCheckState(state);
}


void InputLine::onCheckStateChange(int state) {
    m_input->setDisabled(state == Qt::CheckState::Unchecked);
}

