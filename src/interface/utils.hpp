#ifndef SRC_INTERFACE_UTILS_HPP
#define SRC_INTERFACE_UTILS_HPP

#include "../core/attribute.hpp"

#include <QSpinBox>


constexpr Qt::CheckState checked(bool checked) {
    return checked ? Qt::CheckState::Checked : Qt::CheckState::Unchecked;
}


constexpr int spinBoxAttrMin = std::max(std::numeric_limits<int>::min(), std::numeric_limits<Attribute::IntType>::min());
constexpr int spinBoxAttrMax = std::min(std::numeric_limits<int>::max(), std::numeric_limits<Attribute::IntType>::max());


inline void setupForAttribute(QSpinBox* in) {
    in->setMinimum(spinBoxAttrMin);
    in->setMaximum(spinBoxAttrMax);
}

#endif

