#ifndef SRC_INTERFACE_MAIN_MENU_HPP
#define SRC_INTERFACE_MAIN_MENU_HPP

#include "attribute_editor.hpp"
#include "character_editor.hpp"
#include "interactive_list.hpp"
#include "simulation_executer.hpp"
#include "skill_editor.hpp"

#include <QLayout>
#include <QMainWindow>
#include <QProgressBar>
#include <QPushButton>


class MainMenu: public QMainWindow {
    Q_OBJECT

    SimulationExecuter* m_simulation;

    // User interface
    QWidget* m_mainWidget;
    QVBoxLayout* m_mainLayout;
    // Editor
    QWidget* m_editorWidget;
    QHBoxLayout* m_editorLayout;
    InteractiveList* m_characters;
    QVBoxLayout* m_attrsAndSkills;
    InteractiveList* m_attributes;
    InteractiveList* m_skills;

    // Status bar
    QWidget* m_statusWidget;
    InputLine* m_tasks;
    QHBoxLayout* m_statusLayout;
    QProgressBar* m_statusBar;
    QPushButton* m_startButton;
    QPushButton* m_cancelButton;

    // Dialogs
    CharacterEditor* m_characterEditor;
    AttributeEditor* m_attributeEditor;
    SkillEditor* m_skillEditor;

public:
    MainMenu(QWidget* parent = nullptr);
    ~MainMenu() noexcept;

private slots:
    void launchSimulation();
    void simulationCancelled();
    void simulationFinished(double victoryRate);
    void disableEdition(bool complete = false);
    void enableEdition();

    void addCharacter();
    void modCharacter(int index);
    void delCharacter(int index);
    void addAttribute();
    void modAttribute(int index);
    void addSkill();
    void modSkill(int index);

    void onSelectionUpdated(int oldIndex, int newIndex);

protected:
    void closeEvent(QCloseEvent* e) override;

signals:
    void beforeClose();
};

#endif

