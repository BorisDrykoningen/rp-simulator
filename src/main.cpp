#include "interface/main_menu.hpp"

#include <QApplication>

#include <iostream>


int main(int argc, char** argv) {
    QApplication app(argc, argv);
    MainMenu mainMenu;
    mainMenu.show();
    return app.exec();
}

