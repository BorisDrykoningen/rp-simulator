#ifndef SRC_CORE_SIMULATION_HPP
#define SRC_CORE_SIMULATION_HPP

#include "character.hpp"

#include <atomic>
#include <sstream>
#include <random>
#include <vector>


/**
 * The state of a fight, with its own copy of the characters
 * @see Character
*/
class Simulation {
    /// The characters to manage
    std::vector<Character> m_actors;

    /// The output to write on
    std::ostringstream m_out;

    /// Whether the Simulation is allowed to run
    std::atomic<bool>* m_shouldContinue;

    using RandomEngine = std::minstd_rand;
    /**
     * Gives access to a random engine
     * @return a reference to a thread-local random engine initialized with a
     * random seed. Access should not be synchronized, but the result must not
     * be shared accross threads
    */
    static RandomEngine& randomEngine();

    /**
     * Tells the number of teams where at least one member is still alive
     * @return the number of teams alive
    */
    size_t teamsAlive() const;

public:
    using AttrInt = Attribute::IntType;
    using RandInt = AttrInt;

    /**
     * Creates a new Simulation
    */
    Simulation();

    /**
     * Adds a Character to the Simulation. They play in the order they are
     * submitted
     * @param character the Character to add
    */
    void addCharacter(Character character);

    /**
     * Runs the simulation. Statistics are made available through other queries
    */
    void run();

    /**
     * Logs messages local to that simulation
     * @param args the arguments to display
    */
    template <typename... Args>
    void log(const Args&... args) {
        (m_out << ... << args) << '\n';
    }

    /**
     * Gives the log messages back
     * @return the log messages
    */
    std::string getLogs() const;

    /**
     * Generates a random number, for the callbacks to use
    */
    RandInt random();

    /**
     * Generates a random number, for the callbacks to use
     * @param min the lowest possible value
     * @param max the highest possible value
    */
    RandInt random(RandInt min, RandInt max);

    class Characters {
        std::vector<Character>& m_inner;

    public:
        Characters(std::vector<Character>& v);
        std::vector<Character>::iterator begin();
        std::vector<Character>::iterator end();
    };

    /**
     * Gives access to the characters in this Simulation as an iterable
     * @return the characters in this Simulation
    */
    Characters getCharacters();

    /**
     * Tells who's the winner
     * @return the id of the team that won
    */
    TeamId winner() const;

    /**
     * Tells whether the Simulation should continue. Skills, hooks or any other
     * user code that may take a lot of time to complete (or even never return
     * in some cases) is encouraged to call this periodically. It is useless for
     * fast user code that always returns
     * @return true if the Simulation should continue, false if
     * requestInterruption has been called on the parent (if set)
    */
    bool shouldContinue() const noexcept;

    /**
     * Enables interruptions
     * @param shouldContinue a std::atomic<bool> that becomes false when an interruption is
     * requested
    */
    void enableInterruptions(std::atomic<bool>* shouldContinue) noexcept;
};

#endif

