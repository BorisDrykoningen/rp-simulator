#ifndef SRC_CORE_CHARACTER_HPP
#define SRC_CORE_CHARACTER_HPP

#include "attribute.hpp"
#include "head.hpp"
#include "skill.hpp"
#include "team.hpp"

#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>


/**
 * A character, either a player or a NPC
*/
class Character {
    // Built-in info, used by the simulation engine itself

    /// The character's name, used for display
    std::string m_name;
    /// The character's number of allowed actions
    Attribute m_actions;
    /// The character's health
    Attribute m_health;
    /// The team of the Character
    TeamId m_team;

    // User data, used by the scripts

    // TODO We may use another key type later to avoid copies
    /// The Attributes of the Character
    std::unordered_map<std::string, Attribute> m_attributes;
    /// The Skills of the Character
    std::vector<std::unique_ptr<Skill>> m_skills;
    /// The Head of the Character
    std::unique_ptr<Head> m_head;

public:
    /**
     * Creates a character
     * @param name the Character's name
     * @param team the Character's team
     * @param head the head of the Character
     * @param health the Character's health Attribute
     * @param actions the Character's actions Attribute
    */
    Character(
        std::string name,
        TeamId team,
        std::unique_ptr<Head> head,
        Attribute health,
        Attribute actions = Attribute(0, 1, 1)
    );

    /**
     * Gives access to the Character's name
     * @return the character's name
    */
    std::string_view getName() const;

    /**
     * Tells the Character's team
     * @return the Character's team
    */
    TeamId getTeam() const;

    /**
     * Gives damage to the Character
     * @param damage the damage to deal
    */
    void damage(Attribute::IntType damage);

    /**
     * Heals the Character
     * @param healing the healing to give
    */
    void heal(Attribute::IntType healing);

    // Attribute API

    /**
     * Gets this Character's health special Attribute
     * @return a reference to that Attribute
    */
    Attribute& getHealth();

    /**
     * Gets this Character's health special Attribute
     * @return a reference to that Attribute
    */
    const Attribute& getHealth() const;

    /**
     * Gets this Character's actions special Attribute
     * @return a reference to that Attribute
    */
    Attribute& getActions();

    /**
     * Gets this Character's actions special Attribute
     * @return a reference to that Attribute
    */
    const Attribute& getActions() const;

    /**
     * Gives acces to this Character's Head
     * @return its Head
    */
    Head& getHead();

    /**
     * Gives acces to this Character's Head
     * @return its Head
    */
    const Head& getHead() const;

    /**
     * The Skills of a Character
    */
    class Skills {
        std::vector<std::unique_ptr<Skill>>& m_inner;

    public:
        Skills(std::vector<std::unique_ptr<Skill>>& v);

        /**
         * An iterator over these skills
        */
        class Iterator {
            std::vector<std::unique_ptr<Skill>>::iterator m_inner;

        public:
            using value_type = Skill&;
            Iterator(std::vector<std::unique_ptr<Skill>>::iterator i);
            Iterator operator++();
            Iterator operator++(int);
            Skill& operator*();
            bool operator==(const Iterator& rhs) const;
            bool operator!=(const Iterator& rhs) const;
        };

        Iterator begin();
        Iterator end();
    };

    /**
     * Gives access to this Character's Skills
     * @return an iterable object refering to its Skills
    */
    Skills getSkills();

    /**
     * Adds an Attribute to this Character. Also invalidates references to all
     * Attribute from this Character
     * @param name the new Attribute's name
     * @param value the new Attribute's value
     * @param std::invalid_argument if that name shall be rejected
     * @param std::bad_alloc in case of memory allocation failure
    */
    void addAttribute(std::string name, Attribute value);

    /**
     * Gets an attribute from this character
     * @param name the attribute name
     * @return a reference to that attribute
     * @throw std::invalid_argument if that attribute doesn't exist
    */
    Attribute& getAttribute(const std::string& name);

    /**
     * Gets an attribute from this character
     * @param name the attribute name
     * @return a reference to that attribute
     * @throw std::invalid_argument if that attribute doesn't exist
    */
    const Attribute& getAttribute(const std::string& name) const;

    /**
     * Adds a Skill to the Character
     * @param skill the Skill to add
     * @param std::bad_alloc in case of memory allocation failure
    */
    void addSkill(std::unique_ptr<Skill> skill);
};

#endif

