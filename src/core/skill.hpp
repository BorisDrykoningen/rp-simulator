#ifndef SRC_CORE_SKILL_HPP
#define SRC_CORE_SKILL_HPP

#include "attribute.hpp"
#include "team.hpp"

#include <string>


// Step 1: define Skill as abstract class
// Step 2: define new Skill implementations in builtin
// Step 3: test all of this
// Step 4: implement a Lua skill in script

class Character;
class Simulation;

/**
 * Any ability that a Character can use. Each instance of a Skill is unique to a
 * Character and may have its own state
*/
class Skill {
public:
    /**
     * The usage of the Skill
    */
    enum class Usage {
        Attack,
        Defense,
        Buff,
        Nerf,
        Healing,
        Max = Healing, // Don't insert members after Max!
    };

    /**
     * The type of target accepted by the Skill
    */
    enum class TargetType {
        User,
        Individual,
        Team,
    };

    /**
     * The efficiency of a Skill, by Usage
    */
    class Efficiency {
    public:
        using Scalar = double;

    private:
        Scalar m_vals[static_cast<size_t>(Usage::Max) + 1]{};
        Scalar m_successRate{0.};

    public:
        Efficiency() = default;

        Scalar& operator[](Usage key) noexcept;
        const Scalar& operator[](Usage key) const noexcept;

        Scalar total() const noexcept;

        void setSuccessRate(Scalar successRate) noexcept;
        Scalar getSuccessRate() const noexcept;
    };

    virtual ~Skill() noexcept = default;

    /**
     * Tells the usage of the Skill
     * @return the usage of the Skill
    */
    virtual Usage usage() const = 0;

    /**
     * Tells the type of targets accepted by the Skill
     * @return the type of targets
    */
    virtual TargetType target() const = 0;

    /**
     * Tells the name of the Skill
     * @return a null-terminated string, telling the name of the Skill
    */
    virtual const char* name() const = 0;

    /**
     * Tells whether the Skill can be used right now by user
     * @param user the user of the Skill. Must also be the owner of this
     * instance. This method is also responsible for checking whether user has
     * enough actions left
     * @return true if it can, false otherwise
    */
    virtual bool isAvailable(const Character& user) const = 0;

    /**
     * Tells the expected efficiency of the Skill. The implementer is free to
     * use any convention to compute the efficiency, but should always be
     * consistent, as the same convention shall be used across all skills with
     * the same Usage. Also, the higher the efficiency, the more likely that the
     * Skill will be used
     * @param user the user of the Skill
     * @return the efficiency of the Skill
     * @note this should only be called if target() returns User
     * @note the default implementation throws
    */
    virtual Efficiency efficiency(const Character& user) const;

    /**
     * Tells the expected efficiency of the Skill. The implementer is free to
     * use any convention to compute the efficiency, but should always be
     * consistent, as the same convention shall be used across all skills with
     * the same Usage. Also, the higher the efficiency, the more likely that the
     * Skill will be used
     * @param user the user of the Skill
     * @param target the target of the Skill
     * @return the efficiency of the Skill
     * @note this should only be called if target() returns Individual
     * @note the default implementation throws
    */
    virtual Efficiency efficiency(const Character& user, const Character& target) const;

    /**
     * Tells the expected efficiency of the Skill. The implementer is free to
     * use any convention to compute the efficiency, but should always be
     * consistent, as the same convention shall be used across all skills with
     * the same Usage. Also, the higher the efficiency, the more likely that the
     * Skill will be used
     * @param user the user of the Skill
     * @param simulation the simulation being run
     * @param target the team id of the target of the Skill
     * @return the efficiency of the Skill
     * @note this should only be called if target() returns Team
     * @note the default implementation throws
    */
    virtual Efficiency efficiency(const Character& user, const Simulation& simulation, TeamId target) const;

    /**
     * Actually uses the Skill
     * @param simulation the simulation being run
     * @param user the user of the Skill
     * @note this should only be called if target() returns User
     * @note the default implementation throws
    */
    virtual void use(Simulation& simulation, Character& user);

    /**
     * Actually uses the Skill
     * @param simulation the simulation being run
     * @param user the user of the Skill
     * @param target the target of the Skill
     * @note this should only be called if target() returns Individual
     * @note the default implementation throws
    */
    virtual void use(Simulation& simulation, Character& user, Character& target);

    /**
     * Actually uses the Skill
     * @param simulation the simulation being run
     * @param user the user of the Skill
     * @param target the team id of the target
     * @note this should only be called if target() returns Team
     * @note the default implementation throws
    */
    virtual void use(Simulation& simulation, Character& user, TeamId target);
};

#endif

