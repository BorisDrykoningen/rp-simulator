#ifndef SRC_CORE_ROLL_MODE_HPP
#define SRC_CORE_ROLL_MODE_HPP


namespace skills {
    /// The types of roll supported by the default implementations. You may use
    /// Lua to implement your owns
    enum class RollMode {
        /// From one to the value of the Attribute
        OneToAttr,
        /// From one to one hundred. It's considered a success below the value
        /// of the Attribute
        OneToOneHundred,
    };
}

#endif

