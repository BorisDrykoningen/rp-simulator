#ifndef SRC_CORE_SKILLS_HPP
#define SRC_CORE_SKILLS_HPP

#include "skill_with_initial_roll.hpp"


/// The builtin Skills, implemented in C++
namespace skills {
    /**
     * A basic Skill to perform an attack with a linear damage distribution.
     * Also uses user-defined attributes for roll and damage
    */
    class SingleAttack: public SkillWithInitialRoll {
        std::string m_name;
        AttributeReference m_randDmgAttr;
        AttributeReference m_fixedDmgAttr;

    public:
        /**
         * Creates a new Attack instance
         * @param name the name of the Skill
         * @param rollMode the roll mode to use. Only affects rollAttr
         * @param rollAttr the Attribute to use to roll the dice
         * @param randDmgAttr the name of the Attribute that controls the random
         * damage part
         * @param fixedDmgAttr the name of the Attribute that controls the fixed
         * damage part
         * @note on hit, this will deal 1Drand+fix with rand being the random
         * damages and fix being the fixed damage
        */
        SingleAttack(
            std::string name,
            RollMode rollMode,
            std::string rollAttr,
            std::string randDmgAttr,
            std::string fixedDmgAttr
        );

        Usage usage() const override;
        TargetType target() const override;
        const char* name() const override;
        bool isAvailable(const Character& user) const override;
        Efficiency efficiency(const Character& user, const Character& target) const override;
        void use(Simulation& simulation, Character& user, Character& target) override;

        /**
         * Adds a modifier to all damages (after the dice roll)
         * @param mod the Attribute's name
        */
        void addFixedDamageModifier(std::string mod);

        /**
         * Adds a modifier to random damages (the dice roll's value)
         * @param mod the Attribute's name
        */
        void addRandomDamageModifier(std::string mod);
    };


    /**
     * A basic Skill to perform healing with a linear healing distribution.
     * Heals one single character. May be restricted to only self-healing. Also
     * uses user-defined attributes to perform roll and healing
    */
    class SingleHeal: public SkillWithInitialRoll {
        std::string m_name;
        AttributeReference m_randHealAttr;
        AttributeReference m_fixedHealAttr;
        bool m_canHealAllies;

    public:
        /**
         * Creates a new SingleHeal instance
         * @param name the name of that Skill
         * @param rollMode the way to roll the initial roll
         * @param randHealAttr the name of the Attribute that controls the
         * random part of the heal roll
         * @param fixedHealAttr the name of the Attribute that controls the
         * fixed part of the heal roll
         * @note on success, this will heal by 1Drand+fix with rand being the
         * random heal and fix being the fixed heal
        */
        SingleHeal(
            std::string name,
            RollMode rollMode,
            std::string rollAttr,
            std::string randHealAttr,
            std::string fixedHealAttr,
            bool canHealAllies = true
        );

        Usage usage() const override;
        TargetType target() const override;
        const char* name() const override;
        bool isAvailable(const Character& user) const override;
        Efficiency efficiency(const Character& user) const override;
        Efficiency efficiency(const Character& user, const Character& target) const override;
        void use(Simulation& simulation, Character& user) override;
        void use(Simulation& simulation, Character& user, Character& target) override;

        /**
         * Adds a modifier to all heals (after the dice roll)
         * @param mod the Attribute's name
        */
        void addFixedHealModifier(std::string mod);

        /**
         * Adds a modifier to random heals (the dice roll's value)
         * @param mod the Attribute's name
        */
        void addRandomHealModifier(std::string mod);
    };
}

#endif

