#include "character.hpp"
#include "simulation.hpp"
#include "skill_with_initial_roll.hpp"


skills::SkillWithInitialRoll::SkillWithInitialRoll(std::string mainAttr, skills::RollMode rollMode):
    m_ref(std::move(mainAttr)),
    m_rollMode(rollMode)
{}


void skills::SkillWithInitialRoll::setMainAttribute(std::string mainAttr, skills::RollMode rollMode) {
    m_ref.setMainAttribute(std::move(mainAttr));
    m_rollMode = rollMode;
}


skills::RollMode skills::SkillWithInitialRoll::getRollMode() const {
    return m_rollMode;
}


Attribute::IntType skills::SkillWithInitialRoll::getActualRollValue(const Character& me) const {
    return m_ref.getActualRollValue(me);
}


Attribute::IntType skills::SkillWithInitialRoll::initialRoll(Simulation& s, const Character& me) const {
    return initialRollPrecomputed(s, getActualRollValue(me));
}


Attribute::IntType skills::SkillWithInitialRoll::initialRollPrecomputed(Simulation& s, Attribute::IntType actualRollValue) const {
    switch (m_rollMode) {
    case skills::RollMode::OneToAttr:
        return s.random(1, actualRollValue);

    case skills::RollMode::OneToOneHundred:
        return s.random(1, 100);
    }

    // This code is unreachable but it triggers a warning on gcc if I don't
    // explicitly handle this case
    abort();
}


void skills::SkillWithInitialRoll::addModifier(std::string modAttr) {
    m_ref.addModifier(std::move(modAttr));
}


skills::AttributeReference& skills::SkillWithInitialRoll::asRef() {
    return m_ref;
}


const skills::AttributeReference& skills::SkillWithInitialRoll::asRef() const {
    return m_ref;
}

