#include "character.hpp"

#include <stdexcept>


Character::Character(
    std::string name,
    TeamId team,
    std::unique_ptr<Head> head,
    Attribute health,
    Attribute actions
):
    m_name(std::move(name)),
    m_actions(std::move(actions)),
    m_health(std::move(health)),
    m_team(team),
    m_attributes(),
    m_head(std::move(head))
{}


std::string_view Character::getName() const {
    return m_name;
}


TeamId Character::getTeam() const {
    return m_team;
}


void Character::damage(Attribute::IntType damage) {
    m_health.nerf(damage);
    // Healing after being sent to the minimum should heal instantly instead of
    // filling some underflow
    m_health.clearOverflow();
}


void Character::heal(Attribute::IntType healing) {
    m_health.buff(healing);
    // Getting damaged after being over-healed should damage instantly instead
    // of filling some overflow
    m_health.clearOverflow();
}


Attribute& Character::getHealth() {
    return m_health;
}


const Attribute& Character::getHealth() const {
    return m_health;
}


Attribute& Character::getActions() {
    return m_actions;
}


const Attribute& Character::getActions() const {
    return m_actions;
}


Head& Character::getHead() {
    return *m_head;
}


const Head& Character::getHead() const {
    return *m_head;
}


Character::Skills Character::getSkills() {
    return Skills(m_skills);
}


void Character::addAttribute(std::string name, Attribute value) {
    // Builtin attributes are attributes starting with two underscores. They
    // can't be added as they are either reserved for future use (and therefore
    // non-existant) or already existant. Anyway, trying to create a builtin
    // attribute is probably a bad idea
    if (name.size() >= 2 && name[0] == '_' && name[1] == '_') {
        throw std::invalid_argument(std::move(name));
    }

    m_attributes.emplace(std::move(name), std::move(value));
}


Attribute& Character::getAttribute(const std::string &name) {
    // We assume we get an user-provided Attribute name
    const auto attr = m_attributes.find(name);
    if (attr != m_attributes.end()) {
        return attr->second;
    }

    // It doesn't exist. Maybe it's a builtin attribute?
    if (name.size() < 2 || name[0] != '_' || name[1] != '_') {
        // Nope
        throw std::invalid_argument(name);
    }

    // Well, yes, but actually it might just be a reserved name
    if (name == "__health") {
        return m_health;
    }
    if (name == "__actions") {
        return m_actions;
    }
    throw std::invalid_argument(name);
}


const Attribute& Character::getAttribute(const std::string &name) const {
    return const_cast<Character&>(*this).getAttribute(name);
}


void Character::addSkill(std::unique_ptr<Skill> skill) {
    m_skills.emplace_back(std::move(skill));
}


Character::Skills::Skills(std::vector<std::unique_ptr<Skill>>& v):
    m_inner(v)
{}


Character::Skills::Iterator Character::Skills::begin() {
    return m_inner.begin();
}


Character::Skills::Iterator Character::Skills::end() {
    return m_inner.end();
}


Character::Skills::Iterator::Iterator(std::vector<std::unique_ptr<Skill>>::iterator i):
    m_inner(i)
{}


Character::Skills::Iterator Character::Skills::Iterator::operator++() {
    ++m_inner;

    return *this;
}


Character::Skills::Iterator Character::Skills::Iterator::operator++(int) {
    auto res(*this);
    ++m_inner;

    return res;
}


Skill& Character::Skills::Iterator::operator*() {
    return **m_inner;
}


bool Character::Skills::Iterator::operator==(const Iterator& rhs) const {
    return m_inner == rhs.m_inner;
}


bool Character::Skills::Iterator::operator!=(const Iterator& rhs) const {
    return m_inner != rhs.m_inner;
}

