#include "character.hpp"
#include "heads.hpp"
#include "simulation.hpp"
#include "statistics.hpp"

#include <algorithm>
#include <limits>


heads::Greedy::Greedy(std::size_t dodges, std::string dodgeAttr, RollMode rollMode):
    m_dodges(dodges),
    m_dodgesThisTurn(0),
    m_dodgeAttr(std::move(dodgeAttr)),
    m_rollMode(rollMode)
{}


union PossibleTargets {
    TeamId team;
    Character* character;
};


void heads::Greedy::onMyTurn(Simulation& simulation, Character& me) {
    simulation.log(me.getName(), "'s turn");

    bool didSomething = true;

    while (
        me.getActions().get() > static_cast<Attribute::IntType>(m_dodges - m_dodgesThisTurn) &&
        me.getHealth().get() > 0 &&
        didSomething
    ) {
        simulation.log(me.getName(), " has ", me.getActions().get(), " AP");
        using dLimits = std::numeric_limits<double>;
        constexpr double lowestEff = dLimits::has_infinity ? -dLimits::infinity() : dLimits::lowest();
        double maxEff = lowestEff;

        Skill* bestSkill = nullptr;
        PossibleTargets bestTarget;
        for (Skill& i: me.getSkills()) {
            if (!i.isAvailable(me)) {
                continue;
            }

            decltype(maxEff) eff;
            PossibleTargets target;

            // Don't care about usage or one's role in the team, only efficiency counts
            Skill::Efficiency fullEff;
            switch (i.target()) {
            case Skill::TargetType::User:
                fullEff = i.efficiency(me);
                eff = fullEff.total() * fullEff.getSuccessRate();
                break;

            case Skill::TargetType::Individual:
                eff = lowestEff;
                for (Character& j: simulation.getCharacters()) {
                    // Try not to use a skill on the wrong character (eg
                    // attacking a teammate)
                    switch (i.usage()) {
                    case Skill::Usage::Buff:
                    case Skill::Usage::Defense:
                    case Skill::Usage::Healing:
                        if (j.getTeam() != me.getTeam()) {
                            continue;
                        }
                        break;

                    case Skill::Usage::Nerf:
                    case Skill::Usage::Attack:
                        if (j.getTeam() == me.getTeam()) {
                            continue;
                        }
                        break;
                    }

                    fullEff = i.efficiency(me, j);
                    const auto tmpEff = fullEff.total() * fullEff.getSuccessRate();
                    if (tmpEff > eff) {
                        eff = tmpEff;
                        target.character = &j;
                    }
                }
                break;

            case Skill::TargetType::Team:
                std::vector<TeamId> teams;
                // Team research. Should be fast because the number of teams is
                // expected to be low
                for (const Character& j: simulation.getCharacters()) {
                    if (std::find(teams.begin(), teams.end(), j.getTeam()) == teams.end()) {
                        teams.emplace_back(j.getTeam());
                    }
                }

                eff = lowestEff;
                for (auto j: teams) {
                    fullEff = i.efficiency(me, simulation, j);
                    const auto tmpEff = fullEff.total() * fullEff.getSuccessRate();
                    if (tmpEff > eff) {
                        eff = tmpEff;
                        target.team = j;
                    }
                }
                break;
            }

            if (eff > maxEff) {
                bestSkill = &i;
                bestTarget = target;
                maxEff = eff;
            }
        }

        if (bestSkill) {
            simulation.log("Found a skill to use! ", bestSkill->name());

            switch (bestSkill->target()) {
            case Skill::TargetType::User:
                bestSkill->use(simulation, me);
                break;

            case Skill::TargetType::Individual:
                bestSkill->use(simulation, me, *bestTarget.character);
                break;

            case Skill::TargetType::Team:
                bestSkill->use(simulation, me, bestTarget.team);
                break;
            }
        } else {
            didSomething = false;
        }
    }

    simulation.log("End of ", me.getName(), "'s turn");
}


double heads::Greedy::chancesToHit(
    const Character& me,
    Attribute::IntType rollAttr,
    [[maybe_unused]] Attribute::IntType minDmg,
    Attribute::IntType maxDmg
) const {
    double hitRate;

    // Let's compute the success rate of the Skill (individually)
    switch (m_rollMode) {
    case heads::RollMode::OneToAttr:
        // Unless we can dodge, the enemy always succeeds
        hitRate = 1.;
        break;

    case heads::RollMode::OneToOneHundred:
        hitRate = rollAttr / 100.;
        break;
    }

    if (me.getActions().get() <= 0 || m_dodgesThisTurn >= m_dodges || maxDmg <= 0) {
        // We won't even try
        return hitRate;
    }

    // Let's compute the chances to dodge
    // Here, we know the character is expected to dodge, so we have a dodge
    // attribute
    const auto dodgeAttr = me.getAttribute(m_dodgeAttr).get();
    switch (m_rollMode) {
    case heads::RollMode::OneToAttr:
        // Chances of success of the attack are given by the chances of the
        // attacker to roll a dice lower than the target
        hitRate = compareRandom(rollAttr, dodgeAttr);
        break;

    case heads::RollMode::OneToOneHundred:
        hitRate *= 1. - dodgeAttr / 100.;
        break;
    }

    return hitRate;
}


Head::Decision heads::Greedy::onAttack(
    Simulation &s,
    Character &me,
    Character &enemy,
    Attribute::IntType roll,
    [[maybe_unused]]Attribute::IntType minDmg,
    Attribute::IntType maxDmg
) {
    if (me.getActions().get() <= 0 || m_dodgesThisTurn >= m_dodges || maxDmg <= 0) {
        return Decision::Accept;
    }

    // We have the right do dodge. Will try it
    me.getActions().nerf(1);
    ++m_dodgesThisTurn;

    constexpr Attribute::IntType min = 1;
    Attribute::IntType max;
    Attribute::IntType successAbove;
    switch (m_rollMode) {
    case heads::RollMode::OneToAttr:
        max = me.getAttribute(m_dodgeAttr).get();
        successAbove = roll;
        break;
    case heads::RollMode::OneToOneHundred:
        max = 100;
        successAbove = me.getAttribute(m_dodgeAttr).get();
        break;
    }

    const auto myRoll = s.random(min, max);
    s.log("Rolling a dice... Got ", myRoll);
    if (myRoll >= successAbove) {
        s.log(me.getName(), " dodged the attack!");
        return Decision::Deny;
    }
    s.log(enemy.getName(), " hit ", me.getName());
    return Decision::Accept;
}


void heads::Greedy::onNewTurn(
    [[maybe_unused]] Simulation& s,
    [[maybe_unused]] Character& me
) {
    m_dodgesThisTurn = 0;
}

