#include "character.hpp"
#include "simulation.hpp"
#include "skills.hpp"
#include "statistics.hpp"

#include <stdexcept>


skills::SingleAttack::SingleAttack(std::string name, skills::RollMode rollMode, std::string rollAttr, std::string randDmgAttr, std::string fixedDmgAttr):
    SkillWithInitialRoll(std::move(rollAttr), rollMode),
    m_name(name),
    m_randDmgAttr(std::move(randDmgAttr)),
    m_fixedDmgAttr(std::move(fixedDmgAttr))
{}


Skill::Usage skills::SingleAttack::usage() const {
    return Usage::Attack;
}


Skill::TargetType skills::SingleAttack::target() const {
    return TargetType::Individual;
}


const char* skills::SingleAttack::name() const {
    return m_name.c_str();
}


bool skills::SingleAttack::isAvailable(const Character& user) const {
    return user.getActions().get() >= 1;
}


Skill::Efficiency skills::SingleAttack::efficiency(const Character& user, const Character& target) const {
    // Attacking a dead enemy is like skipping a turn
    if (target.getHealth().get() <= 0) {
        return Efficiency();
    }

    const auto randDmg = m_randDmgAttr.getActualRollValue(user);
    const auto fixedDmg = m_fixedDmgAttr.getActualRollValue(user);
    const auto minDmg = fixedDmg + 1;
    const auto maxDmg = fixedDmg + randDmg;
    if (maxDmg < minDmg) {
        throw std::logic_error("minimum damage greater than maximum damage");
    }

    auto dmg = positiveAverage(minDmg, maxDmg);

    // Add some bonus efficiency if its likely that we kill our enemy. It is
    // likely that we kill our enemy if its health is below the median (at least
    // 50% chances to kill)
    const auto median = static_cast<decltype(dmg)>(minDmg + maxDmg) / 2.;
    if (median >= target.getHealth().get()) {
        dmg *= 2;
    }

    Efficiency eff;
    eff[Usage::Attack] = dmg;
    eff.setSuccessRate(target.getHead().chancesToHit(
        target,
        getActualRollValue(user),
        minDmg,
        maxDmg
    ));
    return eff;
}


void skills::SingleAttack::use(Simulation& simulation, Character& user, Character& target) {
    simulation.log(user.getName(), " attacks ", target.getName());

    user.getActions().nerf(1);

    // First, we need to perform the first roll of dice
    const auto rollAttr = getActualRollValue(user);
    const Attribute::IntType roll = initialRollPrecomputed(simulation, rollAttr);
    if (rollAttr < 1) {
        throw std::logic_error("Attribute lower than 1. Consider using your own skill or setting min value to 1");
    }
    switch (getRollMode()) {
    case RollMode::OneToAttr:
        break;

    case RollMode::OneToOneHundred:
        if (roll > rollAttr) {
            simulation.log("It's a failure");
            return;
        }
        break;
    }

    // Then, we check whether the hit touches the enemy
    const auto randDmg = m_randDmgAttr.getActualRollValue(user);
    const auto fixedDmg = m_fixedDmgAttr.getActualRollValue(user);
    const auto minDmg = fixedDmg + 1;
    const auto maxDmg = fixedDmg + randDmg;
    if (target.getHead().onAttack(simulation, target, user, roll, minDmg, maxDmg) != Head::Decision::Accept) {
        simulation.log("The target avoids the attack!");
        return;
    }

    // We touched it! Now, let's give some damage
    if (maxDmg < minDmg) {
        throw std::logic_error("minimum damage higher than maximum damage");
    }

    Attribute::IntType dmg;
    if (maxDmg < 0) {
        simulation.log("Can't perform damage, max damage value too low");
        dmg = 0;
    } else {
        dmg = simulation.random(minDmg, maxDmg);
        if (dmg < 0) {
            dmg = 0;
        }
    }

    simulation.log(user.getName(), " gives ", dmg, " damage to ", target.getName());
    target.damage(dmg);
    simulation.log(target.getName(), " has ", target.getHealth().get(), " HP now");
}


void skills::SingleAttack::addFixedDamageModifier(std::string mod) {
    m_fixedDmgAttr.addModifier(std::move(mod));
}


void skills::SingleAttack::addRandomDamageModifier(std::string mod) {
    m_randDmgAttr.addModifier(std::move(mod));
}


skills::SingleHeal::SingleHeal(
    std::string name,
    RollMode rollMode,
    std::string rollAttr,
    std::string randHealAttr,
    std::string fixedHealAttr,
    bool canHealAllies
):
    SkillWithInitialRoll(std::move(rollAttr), rollMode),
    m_name(std::move(name)),
    m_randHealAttr(std::move(randHealAttr)),
    m_fixedHealAttr(std::move(fixedHealAttr)),
    m_canHealAllies(canHealAllies)
{}


Skill::Usage skills::SingleHeal::usage() const {
    return Usage::Healing;
}


Skill::TargetType skills::SingleHeal::target() const {
    return m_canHealAllies ? TargetType::Individual : TargetType::User;
}


const char* skills::SingleHeal::name() const {
    return m_name.c_str();
}


bool skills::SingleHeal::isAvailable(const Character& user) const {
    return user.getActions().get() >= 1;
}


Skill::Efficiency skills::SingleHeal::efficiency(const Character& user) const {
    return efficiency(user, user);
}


Skill::Efficiency skills::SingleHeal::efficiency(const Character& user, const Character& target) const {
    if (&user != &target && !m_canHealAllies) {
        throw std::invalid_argument("Capacité indisponible pour un allié!");
    }

    const auto& health = target.getHealth();
    const auto curHealth = health.get();
    const auto maxHealth = health.getMax();
    const auto randHeal = m_randHealAttr.getActualRollValue(user);
    const auto fixedHeal = m_fixedHealAttr.getActualRollValue(user);
    const auto minHeal = fixedHeal + 1;
    const auto maxHeal = fixedHeal + randHeal;

    // No need for corrective functions. If healing overflows, it leads to a
    // lower average
    Efficiency res;
    res[Usage::Healing] = positiveAverageMajored(
        minHeal,
        maxHeal,
        maxHealth - curHealth // the maximum healing authorized
    );

    switch (getRollMode()) {
    case skills::RollMode::OneToAttr:
        // How may them fail?
        res.setSuccessRate(1.);
        break;

    case skills::RollMode::OneToOneHundred:
        res.setSuccessRate(getActualRollValue(user) / 100.);
        break;
    }
    return res;
}


void skills::SingleHeal::use(Simulation& simulation, Character& user) {
    use(simulation, user, user);
}


void skills::SingleHeal::use(Simulation& simulation, Character& user, Character& target) {
    if (&user != &target && !m_canHealAllies) {
        throw std::invalid_argument("Capacité indisponible pour un allié!");
    }

    simulation.log(user.getName(), " utilise ", name(), " sur ", (&user == &target ? "lui-même" : target.getName()));

    user.getActions().nerf(1);

    bool success;
    switch (getRollMode()) {
    case RollMode::OneToAttr:
        // I don't know... who may resist? Perhaps I could extend this with
        // hooks one day...
        success = true;
        break;

    case RollMode::OneToOneHundred:
        const auto attr = getActualRollValue(user);
        const auto roll = initialRollPrecomputed(simulation, attr);
        success = roll <= attr;
        break;
    }

    if (!success) {
        simulation.log("Le soin est un échec");
        return;
    }

    const auto randHeal = m_randHealAttr.getActualRollValue(user);
    const auto fixedHeal = m_fixedHealAttr.getActualRollValue(user);
    const auto minHeal = fixedHeal + 1;
    const auto maxHeal = fixedHeal + randHeal;
    auto amount = simulation.random(minHeal, maxHeal);
    amount = std::max(0, amount);
    simulation.log(amount, " PV(s) récupéré(s)!");

    // TODO: add a hook here. May be used to increase / decrease healing
    // efficiency
    target.heal(amount);
}


void skills::SingleHeal::addFixedHealModifier(std::string mod) {
    m_randHealAttr.addModifier(mod);
    m_fixedHealAttr.addModifier(std::move(mod));
}


void skills::SingleHeal::addRandomHealModifier(std::string mod) {
    m_fixedHealAttr.addModifier(std::move(mod));
}


