#ifndef SRC_CORE_SKILL_WITH_INITIAL_ROLL_HPP
#define SRC_CORE_SKILL_WITH_INITIAL_ROLL_HPP

#include "attribute_reference.hpp"
#include "roll_mode.hpp"
#include "skill.hpp"

#include <vector>


namespace skills {
    /**
     * A Skill those success is determined by an initial dice roll. The initial
     * roll's value is computed using a main Attribute and modifiers attributes.
     * The main Attribute is buffed for each positive modifier and nerfed for
     * each negative modifier. These changes are only local and not actually
     * applied to the main Attribute
     * @note the bounds used for the initial roll are thus defined as the main
     * Attribute's bounds
    */
    class SkillWithInitialRoll: public Skill {
        AttributeReference m_ref;
        RollMode m_rollMode;

    public:
        /**
         * Creates a new SkillWithInitialRoll with no modifiers and an empty
         * main Attribute
        */
        SkillWithInitialRoll() = default;

        /**
         * Creates a new SkillWithInitialRoll with no modifiers
         * @param rollMode the RollMode used
         * @param mainAttr the main Attribute
        */
        SkillWithInitialRoll(std::string mainAttr, skills::RollMode rollMode);

        /**
         * Changes the main Attribute's name
         * @param mainAttr the new main Attribute's name
         * @param rollMode the RollMode used
        */
        void setMainAttribute(std::string mainAttr, skills::RollMode rollMode);

        /**
         * Tells what RollMode has been defined
         * @return the RollMode used
        */
        RollMode getRollMode() const;

        /**
         * Computes the value of the dice's roll
         * @param me the owner of this Skill
         * @param the value of the Attribute used to perform the roll
        */
        Attribute::IntType getActualRollValue(const Character& me) const;

        /**
         * Performs the initial roll
         * @param s the current Simulation
         * @param me the owner of this Skill
         * @return the final value of the roll
        */
        Attribute::IntType initialRoll(Simulation& s, const Character& me) const;

        /**
         * Performs the initial roll. Faster than initialRoll
         * @param s the current Simulation
         * @param actualRollValue the result of a call to getActualRollValue
         * that has not been invalidated by a change to this Skill or its owner
         * @return the final value of the roll
        */
        Attribute::IntType initialRollPrecomputed(Simulation& s, Attribute::IntType actualRollValue) const;

        /**
         * Adds a modifier Attribute
         * @param modAttr the modifier attribute's name
        */
        void addModifier(std::string modAttr);

        /**
         * Returns the reference to the initial roll's attribute
         * @return this' inner reference
        */
        AttributeReference& asRef();

        /**
         * Returns the reference to the initial roll's attribute
         * @return this' inner reference
        */
        const AttributeReference& asRef() const;
    };
}
#endif

