#ifndef SRC_CORE_HEAD_HPP
#define SRC_CORE_HEAD_HPP

#include "attribute.hpp"

#include <string>


class Character;
class Simulation;

/**
 * The "head" of a Character is what helps it to take decisions. It could have
 * been named AI, but it would be technically inaccurate
*/
class Head {
public:
    /**
     * A boolean decision (either accepted or denied)
    */
    enum class Decision {
        Deny = 0,
        Accept = 1,
    };
    virtual ~Head() noexcept = default;

    /**
     * Takes the decision to act while it's the Character's turn to act
     * @param simulation the state of the battlefield
     * @param me the Character this Head works for
    */
    virtual void onMyTurn(Simulation& simulation, Character& me) = 0;

    /**
     * Tells how likely it is that the owner of this Head dodges
     * @param rollAttr the current state of the attribute used to trigger the
     * skill (i.e. rollAttr on builtin skills). On builtin Heads, it is
     * interpreted according to the RollMode set. It is most likely obtained
     * with Attribute::get
     * @param minDmg the minimum damage that this attack may do. As a linear
     * damage distribution is assumed, the caller is allowed to pass a negative
     * value to express that there are multiple rolls that may produce a 0. For
     * instance, -2 means 3 values can produce a 0 (-2, -1, and 0)
     * @param maxDmg the maximum damage dealt. Of course, the caller may pass a
     * value greater than the target's HP
     * @return the chances to dodge
    */
    virtual double chancesToHit(
        const Character& me,
        Attribute::IntType rollAttr,
        Attribute::IntType minDmg,
        Attribute::IntType maxDmg
    ) const = 0;

    /**
     * Takes the decision to either "Accept" an attack or to "Deny" it. Denying
     * the Attack should be based on a fair dice roll using Simulation::random
     * @param simulation the state of the battlefield
     * @param me the Character this Head works for, and therefore, the Character
     * attacked
     * @param enemy the Character who attacks me
     * @param roll the dice roll enemy made
     * @return Accept if me refused to parry or if the parade failed, Deny if me
     * wanted to parry and the parade was successful
    */
    virtual Decision onAttack(
        Simulation& simulation,
        Character& me,
        Character& enemy,
        Attribute::IntType roll,
        Attribute::IntType minDmg,
        Attribute::IntType maxDmg
    ) = 0;

    /**
     * Is called to signal the current Head that a new turn has started. This
     * Head should not take care of anything else than itself. It is not
     * responsbible to reset actions or anything else not directly related to
     * itself. The default implementation does nothing
    */
    virtual void onNewTurn(Simulation& simulation, Character& me);
};

#endif

