#include "skill.hpp"

#include <limits>
#include <stdexcept>


Skill::Efficiency Skill::efficiency([[maybe_unused]] const Character &user) const {
    throw std::logic_error("This skill can't be used on its user!");
}


Skill::Efficiency Skill::efficiency([[maybe_unused]] const Character &user, [[maybe_unused]] const Character &target) const {
    throw std::logic_error("This skill can't be used on another single user!");
}


Skill::Efficiency Skill::efficiency(
    [[maybe_unused]] const Character &user,
    [[maybe_unused]] const Simulation &simulation,
    [[maybe_unused]] TeamId target
) const {
    throw std::logic_error("This skill can't be used on a team!");
}


void Skill::use([[maybe_unused]] Simulation& simulation, [[maybe_unused]] Character &user) {
    throw std::logic_error("This skill can't be used on its user!");
}


void Skill::use(
    [[maybe_unused]] Simulation& simulation,
    [[maybe_unused]] Character &user,
    [[maybe_unused]] Character &target
) {
    throw std::logic_error("This skill can't be used on another single user!");
}


void Skill::use(
    [[maybe_unused]] Simulation &simulation,
    [[maybe_unused]] Character &user,
    [[maybe_unused]] TeamId target
) {
    throw std::logic_error("This skill can't be used on a team!");
}


Skill::Efficiency::Scalar& Skill::Efficiency::operator[](Usage key) noexcept {
    return m_vals[static_cast<size_t>(key)];
}


const Skill::Efficiency::Scalar& Skill::Efficiency::operator[](Usage key) const noexcept {
    return m_vals[static_cast<size_t>(key)];
}


Skill::Efficiency::Scalar Skill::Efficiency::total() const noexcept {
    Scalar total = 0.;
    for (size_t i = 0; i <= static_cast<size_t>(Usage::Max); ++i) {
        total += m_vals[i];
    }

    return total;
}


void Skill::Efficiency::setSuccessRate(Skill::Efficiency::Scalar successRate) noexcept {
    m_successRate = successRate;
}


Skill::Efficiency::Scalar Skill::Efficiency::getSuccessRate() const noexcept {
    return m_successRate;
}

