#include "skill_description.hpp"


SkillDescription::SkillDescription(
    std::string identifier,
    Callback callback,
    std::vector<Entry> desc
):
    m_callback(std::move(callback)),
    m_desc(std::move(desc)),
    m_identifier(std::move(identifier))
{}


void SkillDescription::addParameter(std::string name, std::string label, ValueType type) {
    m_desc.emplace_back(Entry{std::move(name), std::move(label), std::move(type)});
}


std::unique_ptr<Skill> SkillDescription::createSkill(std::string name, const SerializedData& args) {
    return m_callback(std::move(name), args);
}


bool SkillDescription::recognizes(const SerializedData& data) const {
    return data.identifier == m_identifier;
}


const char* SkillDescription::displayName() const {
    return m_identifier.c_str();
}

