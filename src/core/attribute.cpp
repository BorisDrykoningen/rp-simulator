#include "attribute.hpp"

#include <cassert>
#include <stdexcept>


using IntType = Attribute::IntType;

Attribute::Attribute(IntType min, IntType val, IntType max) noexcept:
    m_min(min),
    m_val(val),
    m_mod(0),
    m_max(max)
{}


Attribute::Attribute(IntType val) noexcept: Attribute(noLowerBound, val, noUpperBound) {}


void Attribute::buff(IntType amount) {
    assert(amount >= 0);
    addModifier(amount);
}


void Attribute::nerf(IntType amount) {
    assert(amount >= 0);
    addModifier(-amount);
}


void Attribute::addModifier(IntType amount) {
    if (amount < 0 && noLowerBound - amount > m_mod) {
        throw std::underflow_error("Un malus a causé un dépassement d'entier");
    }
    if (amount > 0 && noUpperBound - amount < m_mod) {
        throw std::overflow_error("Un bonus a causé un dépassement d'entier");
    }

    m_mod += amount;
}


IntType Attribute::get() const {
    if (m_mod > 0) {
        // There is a possibility that simply adding causes an overflow or goes
        // beyond m_max
        if (m_max - m_mod < m_val) {
            return m_max;
        }
    } else if (m_mod < 0) {
        // There is a possibility that adding causes an underflow
        if (m_min - m_mod > m_val) {
            return m_min;
        }
    }

    // We have proven that adding won't cause either an overflow or an underflow
    return m_val + m_mod;
}


void Attribute::clearOverflow() {
    m_mod = getEffectiveModifier();
}


void Attribute::clear() {
    m_mod = 0;
}


Attribute::IntType Attribute::getMin() const {
    return m_min;
}


Attribute::IntType Attribute::getMax() const {
    return m_max;
}


Attribute::IntType Attribute::getTrueModifier() const {
    return m_mod;
}


Attribute::IntType Attribute::getEffectiveModifier() const {
    return get() - m_val;
}


Attribute::IntType Attribute::getValue() const {
    return m_val;
}

