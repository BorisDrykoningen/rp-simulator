#include "attribute_reference.hpp"
#include "character.hpp"
#include "simulation.hpp"


skills::AttributeReference::AttributeReference(std::string mainAttr):
    m_mainAttr(std::move(mainAttr)),
    m_modifiers()
{}


void skills::AttributeReference::setMainAttribute(std::string mainAttr) {
    m_mainAttr = std::move(mainAttr);
}


Attribute::IntType skills::AttributeReference::getActualRollValue(const Character& me) const {
    Attribute mainAttr = me.getAttribute(m_mainAttr);
    for (const auto& i: m_modifiers) {
        mainAttr.addModifier(me.getAttribute(i).get());
    }

    return mainAttr.get();
}


void skills::AttributeReference::addModifier(std::string modAttr) {
    m_modifiers.emplace_back(std::move(modAttr));
}

