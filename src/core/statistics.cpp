#include "statistics.hpp"

#include <cassert>


double positiveAverage(Attribute::IntType min, Attribute::IntType max) {
    assert(min <= max);

    if (min <= 0) {
        if (max <= 0) {
            return 0;
        }

        // The sum of all damage taken in all iterations is
        // s = max * (max + 1) / 2
        // The average damage is
        // avg = s / (-min + max + 1)
        //     = max * (max + 1) / 2(min + max + 1)
        // The line below implicitly casts all operands to double
        return max * (max + 1.) / (2. * (-static_cast<double>(min) + max + 1.));
    }

    // The sum of all damage taken in all iterations is
    // s = ampl * (ampl + 1) / 2 + min * (ampl + 1)
    //   = (ampl / 2 + min) * (ampl + 1)
    // The average damage is
    // avg = s / (ampl + 1)
    //     = ampl / 2 + min
    //     = min + ampl / 2
    const double ampl = max - min;
    return min + ampl / 2.;
}


double positiveAverageMajored(Attribute::IntType min, Attribute::IntType max, Attribute::IntType upperBound) {
    if (min >= upperBound) {
        return upperBound;
    }

    // We start computing the whole sum from 0 to max
    double total;
    if (max > upperBound) {
        const auto sumZeroToUpper = upperBound * (upperBound + 1) / 2;
        // This is a rectangle actually, because above upperBound, it stays at
        // upperBound
        const auto sumUpperToMax = upperBound * (max - upperBound);
        total = sumZeroToUpper + sumUpperToMax;
    } else {
        const auto sumZeroToMax = max * (max + 1) / 2;
        total = sumZeroToMax;
    }

    if (min > 1) {
        const auto sumZeroToMin = min * (min - 1) / 2;
        total -= sumZeroToMin;
    }
    return total / (max - min + 1);
}


double compareRandom(Attribute::IntType max1, Attribute::IntType max2) {
    const double totalCases = static_cast<double>(max1) * max2;

    // We start at X2 = 1, for whom there is 0 possible lower X1. Then goes X2
    // = 2, for whom there is 1 possible lower X1, etc...

    double favorableCases;
    if (max2 <= max1) {
        // X2 increases until its max and we count the values below as
        // possible values for X1
        favorableCases = (max2 - 1.) * max2 / 2.;
    } else {
        const double nonFavorableCases = max1 * (max1 + 1) / 2.;
        favorableCases = totalCases - nonFavorableCases;
    }

    return favorableCases / totalCases;
}

