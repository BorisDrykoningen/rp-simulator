#ifndef SRC_CORE_ATTRIBUTE_HPP
#define SRC_CORE_ATTRIBUTE_HPP

#include <cstdint>
#include <limits>


/**
 * A numeric value, defining a Character's skill
*/
class Attribute {
public:
    /// \brief An unified type for integer representation
    using IntType = int32_t;

private:
    /// \brief The minimum possible value
    IntType m_min;
    /// \brief The character's actual attribute value
    IntType m_val;
    /// \brief The sum of bonuses and maluses
    IntType m_mod;
    /// \brief The maximum possible value
    IntType m_max;

public:
    /**
     * A special argument to pass to the constructor when specifying no upper
     * bound for possible values. In fact, the upper bound is defined by the
     * hardware
    */
    static constexpr IntType noUpperBound = std::numeric_limits<IntType>::max();

    /**
     * A special argument to pass to the constructor when specifying no lower
     * bound for possible values. In fact, the lower bound is defined by the
     * hardware
    */
    static constexpr IntType noLowerBound = std::numeric_limits<IntType>::min();

    /**
     * \brief Creates a new Attribute
     * \param min the minimum possible value for that Attribute
     * \param val the actual value of that Attribute
     * \param max the maximum possible value for that Attribute
    */
    Attribute(IntType min, IntType val, IntType max) noexcept;

    /**
     * \brief Creates a new Attribute with no limits (other than those of
     * IntType)
     * \param val the actual value of that Attribute
    */
    Attribute(IntType val) noexcept;

    /**
     * \brief Adds a bonus to the character
     * \param amount the value of the bonus
     * \note The bonus can't allow the skill to go above its limit
    */
    void buff(IntType amount);

    /**
     * \brief Adds a malus to the character
     * \param amount the value of the bonus
     * \note The malus can't allow the skill to go below its limit
    */
    void nerf(IntType amount);

    /**
     * Applies a modifier to that Attribute. Introduced to fix a problem
     * inherent to Attribute::nerf, because negating the wanted modifier is
     * unsafe if the wanted modifier is std::numeric_limits<IntType>::min()
     * @param mod the modifier to apply. Will be a buff if positive, a nerf if
     * negative, and will be discarded if null
     * @throws std::overflow_error in case of integer overflow
     * @throws std::underflow_error in case of integer underflow
    */
    void addModifier(IntType amount);

    /**
     * \brief Gets the value of that Attribute once the buffs and nerfs taken
     * into account
     * \return that value
    */
    IntType get() const;

    /**
     * Removes any bonus or malus that is out of bounds. Note that this is
     * equivalent to adding either a bonus or malus, removing all other existing
     * bonuses or maluses doesn't clear all bonuses or maluses but keeps that
     * one. You may want to clear it
     * @see clear
    */
    void clearOverflow();

    /**
     * Removes all bonuses and maluses
    */
    void clear();

    /**
     * Tells what is the minimum allowed
     * @return the minimum value that get() is allowed to return. If no lower
     * bound is defined, returns noLowerBound
    */
    IntType getMin() const;

    /**
     * Tells what is the maximum allowed
     * @return the maximum value that get() is allowed to return. If no upper
     * bound is defined, returns noUpperBound
    */
    IntType getMax() const;

    /**
     * Tells what modifier (positive for buffs or negative for nerfs) is applied
     * to this Attribute
     * @return the modifier's value
    */
    IntType getTrueModifier() const;

    /**
     * Tells what is the effective modifier. Like getTrueModifier but is clamped
     * so that getValue() + getEffectiveModifier() == get()
     * @return the effective modifier
    */
    IntType getEffectiveModifier() const;

    /**
     * Tells what is the actual value of this Attribute, without taking the
     * modifier into account
     * @return the actual value, submitted in the constructor
    */
    IntType getValue() const;
};

#endif

