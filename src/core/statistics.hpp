#ifndef SRC_CORE_STATISTICS_HPP
#define SRC_CORE_STATISTICS_HPP

#include "attribute.hpp"


/**
 * Computes the average value of the serie f(x) with:
 * x in [min, max] and
 * f(x) = x if x >= 0
 * f(x) = 0 if x < 0
 * @param min the first value of the serie
 * @param max the last value of the serie
 * @return the average of all terms
 * @note runs in O(1)
*/
double positiveAverage(Attribute::IntType min, Attribute::IntType max);

/**
 * The average value of the serie f(x) with:
 * x in [min, max] and
 * f(x) = x if x <= upperBound
 * f(x) = upperBound if x > upperBound
 * @param min the first value of the serie
 * @param max the last value of the serie
 * @return the average of all terms
 * @note runs in O(1)
*/
double positiveAverageMajored(Attribute::IntType min, Attribute::IntType max, Attribute::IntType upperBound);

/**
 * Computes the chances for a random variable X1 to be lower than a random
 * variable X2
 * @param max1 the upper bound of X1. X1 is in [1, max1]
 * @param max2 the upper bound of X2. X2 is in [1, max2]
 * @return the probability that X1 < X2
*/
double compareRandom(Attribute::IntType max1, Attribute::IntType max2);

#endif

