#include "simulation.hpp"

#include <unordered_set>


Simulation::RandomEngine& Simulation::randomEngine() {
    static std::random_device seeder;
    thread_local RandomEngine engine(seeder());
    // As engine is thread_local, that function can be called concurrently
    // and without synchronization. Its result should not be stored and must not
    // be shared accross threads!
    return engine;
}


size_t Simulation::teamsAlive() const {
    std::unordered_set<TeamId> teamsUp;
    for (const auto& i: m_actors) {
        if (i.getHealth().get() > 0) {
            teamsUp.emplace(i.getTeam());
        }
    }

    return teamsUp.size();
}


Simulation::Simulation():
    m_actors(),
    m_out(),
    m_shouldContinue(nullptr)
{}


void Simulation::addCharacter(Character character) {
    m_actors.emplace_back(std::move(character));
}


void Simulation::run() {
    // I don't plan on inserting many accesses to m_canRun as it may degrade
    // performances. Also, we don't need a lot of responsiveness for
    // interruption handling
    constexpr uint_fast16_t checkInterval = 1000;
    uint_fast16_t beforeCheck = checkInterval;
    while (teamsAlive() > 1) {
        --beforeCheck;
        if (beforeCheck == 0) {
            if (!shouldContinue()) {
                return;
            }
            beforeCheck = checkInterval;
        }

        for (auto& i: m_actors) {
            i.getActions().clear();
            i.getHead().onNewTurn(*this, i);
        }
        for (auto& i: m_actors) {
            i.getHead().onMyTurn(*this, i);
        }
    }
}


std::string Simulation::getLogs() const {
    return m_out.str();
}


Simulation::RandInt Simulation::random() {
    return randomEngine()();
}


Simulation::RandInt Simulation::random(RandInt min, RandInt max) {
    const auto amplitude = max - min + 1;
    return random() % amplitude + min;
}


Simulation::Characters Simulation::getCharacters() {
    return Characters(m_actors);
}


Simulation::Characters::Characters(std::vector<Character>& v):
    m_inner(v)
{}


std::vector<Character>::iterator Simulation::Characters::begin() {
    return m_inner.begin();
}


std::vector<Character>::iterator Simulation::Characters::end() {
    return m_inner.end();
}


TeamId Simulation::winner() const {
    for (const Character& i: m_actors) {
        if (i.getHealth().get() > 0) {
            return i.getTeam();
        }
    }

    throw std::logic_error("No winner");
}


bool Simulation::shouldContinue() const noexcept {
    // if m_shouldContinue == nullptr, then interruptions are disabled
    return !m_shouldContinue || m_shouldContinue->load(std::memory_order_relaxed);
}


void Simulation::enableInterruptions(std::atomic<bool>* shouldContinue) noexcept {
    m_shouldContinue = shouldContinue;
}

