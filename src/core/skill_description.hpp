#ifndef SRC_CORE_SKILL_DESCRIPTION_HPP
#define SRC_CORE_SKILL_DESCRIPTION_HPP

#include "attribute.hpp"
#include "skill.hpp"

#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>


/// The description of the values that parameter a Skill
class SkillDescription {
    friend class AutoForm;

public:
    /// Tells that we are expecting a fixed integer as a parameter
    struct Integer {
        /// The minimum accepted
        std::optional<Attribute::IntType> min;

        /// The minimum accepted
        std::optional<Attribute::IntType> max;
    };

    /// Tells that we expect a string as a parameter. Currently the only way to
    /// declare an attribute
    struct String {};

    /// Tells that we expect only a few predefined values
    struct Enum {
        std::vector<std::string> values;
    };

    /// Tells that we expect a list of attributes used as modifiers
    struct Modifiers {};

    /// The value filled by the user, among all the predefined values submitted
    /// in the associated Enum
    struct EnumValue {
        /// The name of the enum variant
        std::string name;

        /// Its index in the vector submitted
        size_t index;
    };

    /// The modifiers filled by the user
    struct ModifiersValue {
        /// The names of the attributes filled in by the user
        std::vector<std::string> values;
    };

    /// The type of value expected
    using ValueType = std::variant<Integer, String, Enum, Modifiers>;

    /// An actual value, once submitted by the user
    using Value = std::variant<Attribute::IntType, std::string, EnumValue, ModifiersValue>;

    /// A complete set of data, able to build a Skill
    struct SerializedData {
        std::string identifier;
        std::map<std::string, Value> data;
    };

    struct Entry {
        std::string name;
        std::string label;
        ValueType type;
    };

    using Callback = std::function<std::unique_ptr<Skill>(std::string, const SerializedData&)>;

private:
    Callback m_callback;
    std::vector<Entry> m_desc;
    std::string m_identifier;

public:
    SkillDescription() = delete;

    /// Creates a new SkillDescription, uniquely identified by identifier
    SkillDescription(std::string identifier, Callback callback, std::vector<Entry> desc = std::vector<Entry>{});

    /// Adds a new parameter, described to the user with label, refered by the
    /// programmer as name, and of type type
    void addParameter(std::string name, std::string label, ValueType type);

    /// Creates an actual skill from values submitted by an user
    std::unique_ptr<Skill> createSkill(std::string name, const SerializedData& args);

    /// Tells whether the SkillDescription recognizes the data
    bool recognizes(const SerializedData& data) const;

    /// Gives the name of this
    const char* displayName() const;
};

#endif

