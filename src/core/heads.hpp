#ifndef SRC_CORE_HEADS_HPP
#define SRC_CORE_HEADS_HPP

#include "head.hpp"
#include "skills.hpp"


/// The builtin Heads, implemented in C++
namespace heads {
    using skills::RollMode;

    /// A generic greedy algorithm that doesn't know how to parry
    class Greedy: public Head {
        std::size_t m_dodges;
        std::size_t m_dodgesThisTurn;
        std::string m_dodgeAttr;
        RollMode m_rollMode;

    public:
        /**
         * Creates a new greedy algorithm-powered Head
         * @param dodges the number of actions to keep for dodging
         * @param dodgeAttr the attribute used to dodge
         * @param rollMode the RollMode to use to dodge
        */
        Greedy(std::size_t dodges = 0, std::string dodgeAttr = std::string(), RollMode rollMode = RollMode::OneToAttr);
        void onMyTurn(Simulation& simulation, Character& me) override;
        double chancesToHit(
            const Character& me,
            Attribute::IntType rollAttr,
            Attribute::IntType minDmg,
            Attribute::IntType maxDmg
        ) const override;
        Decision onAttack(
            Simulation& s,
            Character& me,
            Character& enemy,
            Attribute::IntType roll,
            Attribute::IntType minDmg,
            Attribute::IntType maxDmg
        ) override;
        void onNewTurn(Simulation& s, Character& me) override;
    };
}
#endif

