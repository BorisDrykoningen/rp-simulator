#ifndef SRC_CORE_ATTRIBUTE_REFERENCE_HPP
#define SRC_CORE_ATTRIBUTE_REFERENCE_HPP

#include "skill.hpp"

#include <vector>


namespace skills {
    /**
     * When a Skill uses an Attribute for whatever reason, there are two ways to
     * make that Skill more efficient:
     * - directly buff the Attribute it uses as main Attribute
     * - use modifiers
     *
     * The later allows a more fine-grained control over bonuses and maluses
     * while making it less difficult for end-users. Indeed, if one wants to
     * increase the physical damage, just create a physical damage Attribute and
     * use it as a modifier for the damages of all physical attacks
    */
    class AttributeReference {
        std::string m_mainAttr;
        std::vector<std::string> m_modifiers;

    public:
        /**
         * Creates a new AttributeReference with no modifiers and an empty
         * main Attribute
        */
        AttributeReference() = default;

        /**
         * Creates a new AttributeReference with no modifiers
         * @param mainAttr the main Attribute
        */
        AttributeReference(std::string mainAttr);

        /**
         * Changes the main Attribute's name
         * @param mainAttr the new main Attribute's name
        */
        void setMainAttribute(std::string mainAttr);

        /**
         * Computes the value of the dice's roll
         * @param me the owner of this Skill
         * @param the value of the Attribute used to perform the roll
        */
        Attribute::IntType getActualRollValue(const Character& me) const;

        /**
         * Adds a modifier Attribute
         * @param modAttr the modifier attribute's name
        */
        void addModifier(std::string modAttr);
    };
}
#endif

