#ifndef SRC_RUNTIME_THREADED_RUNTIME_HPP
#define SRC_RUNTIME_THREADED_RUNTIME_HPP

#include "../core/simulation.hpp"

#include <atomic>
#include <iostream>
#include <optional>
#include <thread>
#include <vector>


/**
 * An object that runs simulations and tells the current amount of work
 * remaining
*/
class ThreadedRuntime {
    std::vector<std::thread> m_threads;
    std::atomic<ssize_t> m_pending;
    std::atomic<size_t> m_completed;
    std::atomic<size_t> m_victories;
    std::atomic<bool> m_keepRunning;
    size_t m_total;
    TeamId m_allied;

    template <typename F>
    static void worker(ThreadedRuntime* rt, F builder);

public:
    /**
     * Creates a new ThreadedRuntime
     * @param allied the team to watch for victories
    */
    ThreadedRuntime(TeamId allied);

    /**
     * Launchs the ThreadedRuntime running the given simulation builder
     * @param builder the builder to generate simulations. Must be callable and
     * thread safe so that threads can call it concurrently. Must be copyable
     * @param the number of threads to run. 0 is treated as automatic. In this
     * case, if automatic detection is unavailable, we fall back to an
     * implementation defined method
     * @param the number of simulations to run
    */
    template <typename F>
    void run(F builder, size_t threads, size_t simulations);

    /**
     * Waits for all the simulations to complete
    */
    void wait();

    /**
     * Interrupts the simulations
     * @warning This request is asynchronous. You must wait for the simulations
     * to actually terminate
     * @see wait
    */
    void requestInterruption() noexcept;

    /**
     * Tells the number of completed simulations
     * @return the number of completed simulations
    */
    size_t completed() const;

    /**
     * Tells the number of completed simulations (as a percentage)
     * @return the percentage of completed simulations
    */
    double percent() const;

    /**
     * Tells the probability for the watched team to win
     * @return the observed victory frenquency (from 0 to 1)
    */
    double victoryRate() const;
};

#include "threaded_runtime.inl"

#endif


