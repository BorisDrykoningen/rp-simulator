#include "threaded_runtime.hpp"


ThreadedRuntime::ThreadedRuntime(TeamId allied): m_allied(allied) {}


void ThreadedRuntime::wait() {
    for (auto& i: m_threads) {
        i.join();
    }
}


void ThreadedRuntime::requestInterruption() noexcept {
    m_keepRunning.store(false, std::memory_order_relaxed);
}


size_t ThreadedRuntime::completed() const {
    return m_completed;
}


double ThreadedRuntime::percent() const {
    if (!m_total) {
        return 0.;
    }
    return 100. * static_cast<double>(completed()) / m_total;
}


double ThreadedRuntime::victoryRate() const {
    return static_cast<double>(m_victories) / m_total;
}

