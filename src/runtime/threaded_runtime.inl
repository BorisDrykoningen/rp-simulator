template <typename F>
void ThreadedRuntime::run(F builder, size_t threads, size_t simulations) {
    if (threads == 0) {
        threads = std::thread::hardware_concurrency();
        if (threads == 0) {
            // Because why not?
            threads = 4;
        }
    }
    if (simulations < threads) {
        threads = simulations;
    }

    m_total = simulations;
    m_pending = m_total;
    m_completed = 0;
    m_victories = 0;
    m_keepRunning = true;
    m_threads.clear();

    m_threads.reserve(threads);
    for (size_t i = 0; i < threads; ++i) {
        F j = builder;
        m_threads.emplace_back(worker<F>, this, j);
    }
}


template <typename F>
void ThreadedRuntime::worker(ThreadedRuntime* rt, F builder) {
    do {
        const ssize_t pending = rt->m_pending.fetch_sub(1);
        if (pending < 1) {
            return;
        }

        std::optional<Simulation> s;
        try {
            s = builder();
            auto& sv = s.value();
            sv.enableInterruptions(&rt->m_keepRunning);
            sv.run();
            rt->m_victories += static_cast<size_t>(s.value().winner() == rt->m_allied);
        } catch (const std::exception& err) {
            std::cerr << "Simulation failed: " << err.what() << '\n';
            if (s.has_value()) {
                std::cerr << "Simulation's output: " << s.value().getLogs();
            }
        } catch (...) {
            std::cerr << "Simulation failed with an unknown error\n";
            if (s.has_value()) {
                std::cerr << "Simulation's output: " << s.value().getLogs();
            }
        }

        ++rt->m_completed;
    } while (rt->m_keepRunning);
}

